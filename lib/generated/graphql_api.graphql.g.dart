// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'graphql_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchListing$Query$ListingPaginator
    _$SearchListing$Query$ListingPaginatorFromJson(Map<String, dynamic> json) {
  return SearchListing$Query$ListingPaginator()
    ..paginatorInfo = json['paginatorInfo'] == null
        ? null
        : SearchListing$Query$ListingPaginator$PaginatorInfo.fromJson(
            json['paginatorInfo'] as Map<String, dynamic>)
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SearchListing$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SearchListing$Query$ListingPaginatorToJson(
        SearchListing$Query$ListingPaginator instance) =>
    <String, dynamic>{
      'paginatorInfo': instance.paginatorInfo?.toJson(),
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

SearchListing$Query _$SearchListing$QueryFromJson(Map<String, dynamic> json) {
  return SearchListing$Query()
    ..searchListing = json['searchListing'] == null
        ? null
        : SearchListing$Query$ListingPaginator.fromJson(
            json['searchListing'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SearchListing$QueryToJson(
        SearchListing$Query instance) =>
    <String, dynamic>{
      'searchListing': instance.searchListing?.toJson(),
    };

SearchListing$Query$ListingPaginator$PaginatorInfo
    _$SearchListing$Query$ListingPaginator$PaginatorInfoFromJson(
        Map<String, dynamic> json) {
  return SearchListing$Query$ListingPaginator$PaginatorInfo()
    ..hasMorePages = json['hasMorePages'] as bool;
}

Map<String, dynamic> _$SearchListing$Query$ListingPaginator$PaginatorInfoToJson(
        SearchListing$Query$ListingPaginator$PaginatorInfo instance) =>
    <String, dynamic>{
      'hasMorePages': instance.hasMorePages,
    };

SearchListing$Query$ListingPaginator$Listing$Photo
    _$SearchListing$Query$ListingPaginator$Listing$PhotoFromJson(
        Map<String, dynamic> json) {
  return SearchListing$Query$ListingPaginator$Listing$Photo()
    ..url = json['url'] as String;
}

Map<String, dynamic> _$SearchListing$Query$ListingPaginator$Listing$PhotoToJson(
        SearchListing$Query$ListingPaginator$Listing$Photo instance) =>
    <String, dynamic>{
      'url': instance.url,
    };

SearchListing$Query$ListingPaginator$Listing
    _$SearchListing$Query$ListingPaginator$ListingFromJson(
        Map<String, dynamic> json) {
  return SearchListing$Query$ListingPaginator$Listing()
    ..id = json['id'] as String
    ..title = json['title'] as String
    ..address = json['address'] as String
    ..bedrooms = json['bedrooms'] as int
    ..bathrooms = json['bathrooms'] as int
    ..price = json['price'] as int
    ..floor_area = (json['floor_area'] as num)?.toDouble()
    ..isFavorite = json['isFavorite'] as bool
    ..photos = (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : SearchListing$Query$ListingPaginator$Listing$Photo.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SearchListing$Query$ListingPaginator$ListingToJson(
        SearchListing$Query$ListingPaginator$Listing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'address': instance.address,
      'bedrooms': instance.bedrooms,
      'bathrooms': instance.bathrooms,
      'price': instance.price,
      'floor_area': instance.floor_area,
      'isFavorite': instance.isFavorite,
      'photos': instance.photos?.map((e) => e?.toJson())?.toList(),
    };

SearchListingArguments _$SearchListingArgumentsFromJson(
    Map<String, dynamic> json) {
  return SearchListingArguments(
    amenities: (json['amenities'] as List)?.map((e) => e as int)?.toList(),
    bathrooms: json['bathrooms'] as int,
    bathrooms_more_than: json['bathrooms_more_than'] as int,
    bedrooms: json['bedrooms'] as int,
    bedrooms_more_than: json['bedrooms_more_than'] as int,
    first: json['first'] as int,
    listing_type_id: json['listing_type_id'] as int,
    page: json['page'] as int,
    parking_type_id: json['parking_type_id'] as int,
    pet_friendly: json['pet_friendly'] as int,
    price_from: json['price_from'] as int,
    price_to: json['price_to'] as int,
    search: json['search'] as String,
    available: json['available'] as bool,
    couples: json['couples'] as int,
    furnished: json['furnished'] as bool,
    orderBy:
        _$enumDecodeNullable(_$SearchListing$SortOrderEnumMap, json['orderBy']),
  );
}

Map<String, dynamic> _$SearchListingArgumentsToJson(
        SearchListingArguments instance) =>
    <String, dynamic>{
      'amenities': instance.amenities,
      'bathrooms': instance.bathrooms,
      'bathrooms_more_than': instance.bathrooms_more_than,
      'bedrooms': instance.bedrooms,
      'bedrooms_more_than': instance.bedrooms_more_than,
      'first': instance.first,
      'listing_type_id': instance.listing_type_id,
      'page': instance.page,
      'parking_type_id': instance.parking_type_id,
      'pet_friendly': instance.pet_friendly,
      'price_from': instance.price_from,
      'price_to': instance.price_to,
      'search': instance.search,
      'available': instance.available,
      'couples': instance.couples,
      'furnished': instance.furnished,
      'orderBy': _$SearchListing$SortOrderEnumMap[instance.orderBy],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$SearchListing$SortOrderEnumMap = {
  SearchListing$SortOrder.ASC: 'ASC',
  SearchListing$SortOrder.DESC: 'DESC',
  SearchListing$SortOrder.ARTEMIS_UNKNOWN: 'ARTEMIS_UNKNOWN',
};

MessagedListings$Query$User$Listing
    _$MessagedListings$Query$User$ListingFromJson(Map<String, dynamic> json) {
  return MessagedListings$Query$User$Listing()
    ..paginatorInfo = json['paginatorInfo'] == null
        ? null
        : MessagedListings$Query$ListingPaginator$PaginatorInfo.fromJson(
            json['paginatorInfo'] as Map<String, dynamic>)
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : MessagedListings$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$MessagedListings$Query$User$ListingToJson(
        MessagedListings$Query$User$Listing instance) =>
    <String, dynamic>{
      'paginatorInfo': instance.paginatorInfo?.toJson(),
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

MessagedListings$Query$User _$MessagedListings$Query$UserFromJson(
    Map<String, dynamic> json) {
  return MessagedListings$Query$User()
    ..messaged_listings = (json['messaged_listings'] as List)
        ?.map((e) => e == null
            ? null
            : MessagedListings$Query$User$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$MessagedListings$Query$UserToJson(
        MessagedListings$Query$User instance) =>
    <String, dynamic>{
      'messaged_listings':
          instance.messaged_listings?.map((e) => e?.toJson())?.toList(),
    };

MessagedListings$Query _$MessagedListings$QueryFromJson(
    Map<String, dynamic> json) {
  return MessagedListings$Query()
    ..me = json['me'] == null
        ? null
        : MessagedListings$Query$User.fromJson(
            json['me'] as Map<String, dynamic>);
}

Map<String, dynamic> _$MessagedListings$QueryToJson(
        MessagedListings$Query instance) =>
    <String, dynamic>{
      'me': instance.me?.toJson(),
    };

MessagedListings$Query$ListingPaginator$PaginatorInfo
    _$MessagedListings$Query$ListingPaginator$PaginatorInfoFromJson(
        Map<String, dynamic> json) {
  return MessagedListings$Query$ListingPaginator$PaginatorInfo()
    ..hasMorePages = json['hasMorePages'] as bool;
}

Map<String, dynamic>
    _$MessagedListings$Query$ListingPaginator$PaginatorInfoToJson(
            MessagedListings$Query$ListingPaginator$PaginatorInfo instance) =>
        <String, dynamic>{
          'hasMorePages': instance.hasMorePages,
        };

MessagedListings$Query$ListingPaginator$Listing$Photo
    _$MessagedListings$Query$ListingPaginator$Listing$PhotoFromJson(
        Map<String, dynamic> json) {
  return MessagedListings$Query$ListingPaginator$Listing$Photo()
    ..url = json['url'] as String;
}

Map<String, dynamic>
    _$MessagedListings$Query$ListingPaginator$Listing$PhotoToJson(
            MessagedListings$Query$ListingPaginator$Listing$Photo instance) =>
        <String, dynamic>{
          'url': instance.url,
        };

MessagedListings$Query$ListingPaginator$Listing
    _$MessagedListings$Query$ListingPaginator$ListingFromJson(
        Map<String, dynamic> json) {
  return MessagedListings$Query$ListingPaginator$Listing()
    ..id = json['id'] as String
    ..title = json['title'] as String
    ..address = json['address'] as String
    ..bedrooms = json['bedrooms'] as int
    ..bathrooms = json['bathrooms'] as int
    ..price = json['price'] as int
    ..floor_area = (json['floor_area'] as num)?.toDouble()
    ..isFavorite = json['isFavorite'] as bool
    ..photos = (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : MessagedListings$Query$ListingPaginator$Listing$Photo.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$MessagedListings$Query$ListingPaginator$ListingToJson(
        MessagedListings$Query$ListingPaginator$Listing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'address': instance.address,
      'bedrooms': instance.bedrooms,
      'bathrooms': instance.bathrooms,
      'price': instance.price,
      'floor_area': instance.floor_area,
      'isFavorite': instance.isFavorite,
      'photos': instance.photos?.map((e) => e?.toJson())?.toList(),
    };

ForgotPassword$Mutation$ForgotPasswordResponse
    _$ForgotPassword$Mutation$ForgotPasswordResponseFromJson(
        Map<String, dynamic> json) {
  return ForgotPassword$Mutation$ForgotPasswordResponse()
    ..status = json['status'] as String
    ..message = json['message'] as String;
}

Map<String, dynamic> _$ForgotPassword$Mutation$ForgotPasswordResponseToJson(
        ForgotPassword$Mutation$ForgotPasswordResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
    };

ForgotPassword$Mutation _$ForgotPassword$MutationFromJson(
    Map<String, dynamic> json) {
  return ForgotPassword$Mutation()
    ..forgotPassword = json['forgotPassword'] == null
        ? null
        : ForgotPassword$Mutation$ForgotPasswordResponse.fromJson(
            json['forgotPassword'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ForgotPassword$MutationToJson(
        ForgotPassword$Mutation instance) =>
    <String, dynamic>{
      'forgotPassword': instance.forgotPassword?.toJson(),
    };

ForgotPasswordArguments _$ForgotPasswordArgumentsFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordArguments(
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$ForgotPasswordArgumentsToJson(
        ForgotPasswordArguments instance) =>
    <String, dynamic>{
      'email': instance.email,
    };

Unsave$Mutation$User _$Unsave$Mutation$UserFromJson(Map<String, dynamic> json) {
  return Unsave$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$Unsave$Mutation$UserToJson(
        Unsave$Mutation$User instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

Unsave$Mutation _$Unsave$MutationFromJson(Map<String, dynamic> json) {
  return Unsave$Mutation()
    ..updateFavoriteList = json['updateFavoriteList'] == null
        ? null
        : Unsave$Mutation$User.fromJson(
            json['updateFavoriteList'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Unsave$MutationToJson(Unsave$Mutation instance) =>
    <String, dynamic>{
      'updateFavoriteList': instance.updateFavoriteList?.toJson(),
    };

UnsaveArguments _$UnsaveArgumentsFromJson(Map<String, dynamic> json) {
  return UnsaveArguments(
    listing_id: json['listing_id'] as String,
  );
}

Map<String, dynamic> _$UnsaveArgumentsToJson(UnsaveArguments instance) =>
    <String, dynamic>{
      'listing_id': instance.listing_id,
    };

Login$Mutation$AuthPayload$User _$Login$Mutation$AuthPayload$UserFromJson(
    Map<String, dynamic> json) {
  return Login$Mutation$AuthPayload$User()
    ..id = json['id'] as String
    ..first_name = json['first_name'] as String
    ..last_name = json['last_name'] as String
    ..email = json['email'] as String
    ..mobile_phone = json['mobile_phone'] as String
    ..avatar = json['avatar'] as String;
}

Map<String, dynamic> _$Login$Mutation$AuthPayload$UserToJson(
        Login$Mutation$AuthPayload$User instance) =>
    <String, dynamic>{
      'id': instance.id,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'email': instance.email,
      'mobile_phone': instance.mobile_phone,
      'avatar': instance.avatar,
    };

Login$Mutation$AuthPayload _$Login$Mutation$AuthPayloadFromJson(
    Map<String, dynamic> json) {
  return Login$Mutation$AuthPayload()
    ..access_token = json['access_token'] as String
    ..user = json['user'] == null
        ? null
        : Login$Mutation$AuthPayload$User.fromJson(
            json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$Mutation$AuthPayloadToJson(
        Login$Mutation$AuthPayload instance) =>
    <String, dynamic>{
      'access_token': instance.access_token,
      'user': instance.user?.toJson(),
    };

Login$Mutation _$Login$MutationFromJson(Map<String, dynamic> json) {
  return Login$Mutation()
    ..login = json['login'] == null
        ? null
        : Login$Mutation$AuthPayload.fromJson(
            json['login'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$MutationToJson(Login$Mutation instance) =>
    <String, dynamic>{
      'login': instance.login?.toJson(),
    };

LoginArguments _$LoginArgumentsFromJson(Map<String, dynamic> json) {
  return LoginArguments(
    username: json['username'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$LoginArgumentsToJson(LoginArguments instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
    };

Register$Mutation$RegisterResponse _$Register$Mutation$RegisterResponseFromJson(
    Map<String, dynamic> json) {
  return Register$Mutation$RegisterResponse()
    ..status = _$enumDecodeNullable(
        _$Register$Mutation$RegisterResponse$RegisterStatusesEnumMap,
        json['status'],
        unknownValue: Register$Mutation$RegisterResponse$RegisterStatuses
            .ARTEMIS_UNKNOWN);
}

Map<String, dynamic> _$Register$Mutation$RegisterResponseToJson(
        Register$Mutation$RegisterResponse instance) =>
    <String, dynamic>{
      'status': _$Register$Mutation$RegisterResponse$RegisterStatusesEnumMap[
          instance.status],
    };

const _$Register$Mutation$RegisterResponse$RegisterStatusesEnumMap = {
  Register$Mutation$RegisterResponse$RegisterStatuses.MUST_VERIFY_EMAIL:
      'MUST_VERIFY_EMAIL',
  Register$Mutation$RegisterResponse$RegisterStatuses.SUCCESS: 'SUCCESS',
  Register$Mutation$RegisterResponse$RegisterStatuses.ARTEMIS_UNKNOWN:
      'ARTEMIS_UNKNOWN',
};

Register$Mutation _$Register$MutationFromJson(Map<String, dynamic> json) {
  return Register$Mutation()
    ..register = json['register'] == null
        ? null
        : Register$Mutation$RegisterResponse.fromJson(
            json['register'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Register$MutationToJson(Register$Mutation instance) =>
    <String, dynamic>{
      'register': instance.register?.toJson(),
    };

RegisterArguments _$RegisterArgumentsFromJson(Map<String, dynamic> json) {
  return RegisterArguments(
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
    password_confirmation: json['password_confirmation'] as String,
  );
}

Map<String, dynamic> _$RegisterArgumentsToJson(RegisterArguments instance) =>
    <String, dynamic>{
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'email': instance.email,
      'password': instance.password,
      'password_confirmation': instance.password_confirmation,
    };

Property$Query$Listing$Amenity _$Property$Query$Listing$AmenityFromJson(
    Map<String, dynamic> json) {
  return Property$Query$Listing$Amenity()..name = json['name'] as String;
}

Map<String, dynamic> _$Property$Query$Listing$AmenityToJson(
        Property$Query$Listing$Amenity instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

Property$Query$Listing$ListingDescription
    _$Property$Query$Listing$ListingDescriptionFromJson(
        Map<String, dynamic> json) {
  return Property$Query$Listing$ListingDescription()
    ..language = json['language'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$Property$Query$Listing$ListingDescriptionToJson(
        Property$Query$Listing$ListingDescription instance) =>
    <String, dynamic>{
      'language': instance.language,
      'description': instance.description,
    };

Property$Query$Listing$Photo _$Property$Query$Listing$PhotoFromJson(
    Map<String, dynamic> json) {
  return Property$Query$Listing$Photo()..url = json['url'] as String;
}

Map<String, dynamic> _$Property$Query$Listing$PhotoToJson(
        Property$Query$Listing$Photo instance) =>
    <String, dynamic>{
      'url': instance.url,
    };

Property$Query$Listing _$Property$Query$ListingFromJson(
    Map<String, dynamic> json) {
  return Property$Query$Listing()
    ..id = json['id'] as String
    ..title = json['title'] as String
    ..address = json['address'] as String
    ..pet_friendly = json['pet_friendly'] as bool
    ..isFavorite = json['isFavorite'] as bool
    ..bedrooms = json['bedrooms'] as int
    ..bathrooms = json['bathrooms'] as int
    ..price = json['price'] as int
    ..reference = json['reference'] as String
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..floor_area = (json['floor_area'] as num)?.toDouble()
    ..isMessaged = json['isMessaged'] as bool
    ..amenities = (json['amenities'] as List)
        ?.map((e) => e == null
            ? null
            : Property$Query$Listing$Amenity.fromJson(
                e as Map<String, dynamic>))
        ?.toList()
    ..descriptions = (json['descriptions'] as List)
        ?.map((e) => e == null
            ? null
            : Property$Query$Listing$ListingDescription.fromJson(
                e as Map<String, dynamic>))
        ?.toList()
    ..photos = (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : Property$Query$Listing$Photo.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Property$Query$ListingToJson(
        Property$Query$Listing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'address': instance.address,
      'pet_friendly': instance.pet_friendly,
      'isFavorite': instance.isFavorite,
      'bedrooms': instance.bedrooms,
      'bathrooms': instance.bathrooms,
      'price': instance.price,
      'reference': instance.reference,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'floor_area': instance.floor_area,
      'isMessaged': instance.isMessaged,
      'amenities': instance.amenities?.map((e) => e?.toJson())?.toList(),
      'descriptions': instance.descriptions?.map((e) => e?.toJson())?.toList(),
      'photos': instance.photos?.map((e) => e?.toJson())?.toList(),
    };

Property$Query _$Property$QueryFromJson(Map<String, dynamic> json) {
  return Property$Query()
    ..listing = json['listing'] == null
        ? null
        : Property$Query$Listing.fromJson(
            json['listing'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Property$QueryToJson(Property$Query instance) =>
    <String, dynamic>{
      'listing': instance.listing?.toJson(),
    };

PropertyArguments _$PropertyArgumentsFromJson(Map<String, dynamic> json) {
  return PropertyArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$PropertyArgumentsToJson(PropertyArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

MakeEnquiry$Mutation$Lead _$MakeEnquiry$Mutation$LeadFromJson(
    Map<String, dynamic> json) {
  return MakeEnquiry$Mutation$Lead()..id = json['id'] as String;
}

Map<String, dynamic> _$MakeEnquiry$Mutation$LeadToJson(
        MakeEnquiry$Mutation$Lead instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

MakeEnquiry$Mutation _$MakeEnquiry$MutationFromJson(Map<String, dynamic> json) {
  return MakeEnquiry$Mutation()
    ..createLead = json['createLead'] == null
        ? null
        : MakeEnquiry$Mutation$Lead.fromJson(
            json['createLead'] as Map<String, dynamic>);
}

Map<String, dynamic> _$MakeEnquiry$MutationToJson(
        MakeEnquiry$Mutation instance) =>
    <String, dynamic>{
      'createLead': instance.createLead?.toJson(),
    };

MakeEnquiryArguments _$MakeEnquiryArgumentsFromJson(Map<String, dynamic> json) {
  return MakeEnquiryArguments(
    listing_id: json['listing_id'] as int,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    email: json['email'] as String,
    phone: json['phone'] as String,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$MakeEnquiryArgumentsToJson(
        MakeEnquiryArguments instance) =>
    <String, dynamic>{
      'listing_id': instance.listing_id,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'email': instance.email,
      'phone': instance.phone,
      'message': instance.message,
    };

Home$Query$AvailableNow _$Home$Query$AvailableNowFromJson(
    Map<String, dynamic> json) {
  return Home$Query$AvailableNow()
    ..paginatorInfo = json['paginatorInfo'] == null
        ? null
        : Home$Query$ListingPaginator$PaginatorInfo.fromJson(
            json['paginatorInfo'] as Map<String, dynamic>)
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : Home$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Home$Query$AvailableNowToJson(
        Home$Query$AvailableNow instance) =>
    <String, dynamic>{
      'paginatorInfo': instance.paginatorInfo?.toJson(),
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

Home$Query$PetFriendly _$Home$Query$PetFriendlyFromJson(
    Map<String, dynamic> json) {
  return Home$Query$PetFriendly()
    ..paginatorInfo = json['paginatorInfo'] == null
        ? null
        : Home$Query$ListingPaginator$PaginatorInfo.fromJson(
            json['paginatorInfo'] as Map<String, dynamic>)
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : Home$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Home$Query$PetFriendlyToJson(
        Home$Query$PetFriendly instance) =>
    <String, dynamic>{
      'paginatorInfo': instance.paginatorInfo?.toJson(),
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

Home$Query _$Home$QueryFromJson(Map<String, dynamic> json) {
  return Home$Query()
    ..availableNow = json['availableNow'] == null
        ? null
        : Home$Query$AvailableNow.fromJson(
            json['availableNow'] as Map<String, dynamic>)
    ..petFriendly = json['petFriendly'] == null
        ? null
        : Home$Query$PetFriendly.fromJson(
            json['petFriendly'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Home$QueryToJson(Home$Query instance) =>
    <String, dynamic>{
      'availableNow': instance.availableNow?.toJson(),
      'petFriendly': instance.petFriendly?.toJson(),
    };

Home$Query$ListingPaginator$PaginatorInfo
    _$Home$Query$ListingPaginator$PaginatorInfoFromJson(
        Map<String, dynamic> json) {
  return Home$Query$ListingPaginator$PaginatorInfo()
    ..hasMorePages = json['hasMorePages'] as bool;
}

Map<String, dynamic> _$Home$Query$ListingPaginator$PaginatorInfoToJson(
        Home$Query$ListingPaginator$PaginatorInfo instance) =>
    <String, dynamic>{
      'hasMorePages': instance.hasMorePages,
    };

Home$Query$ListingPaginator$Listing$Photo
    _$Home$Query$ListingPaginator$Listing$PhotoFromJson(
        Map<String, dynamic> json) {
  return Home$Query$ListingPaginator$Listing$Photo()
    ..url = json['url'] as String;
}

Map<String, dynamic> _$Home$Query$ListingPaginator$Listing$PhotoToJson(
        Home$Query$ListingPaginator$Listing$Photo instance) =>
    <String, dynamic>{
      'url': instance.url,
    };

Home$Query$ListingPaginator$Listing
    _$Home$Query$ListingPaginator$ListingFromJson(Map<String, dynamic> json) {
  return Home$Query$ListingPaginator$Listing()
    ..id = json['id'] as String
    ..title = json['title'] as String
    ..address = json['address'] as String
    ..bedrooms = json['bedrooms'] as int
    ..bathrooms = json['bathrooms'] as int
    ..price = json['price'] as int
    ..floor_area = (json['floor_area'] as num)?.toDouble()
    ..isFavorite = json['isFavorite'] as bool
    ..photos = (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : Home$Query$ListingPaginator$Listing$Photo.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Home$Query$ListingPaginator$ListingToJson(
        Home$Query$ListingPaginator$Listing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'address': instance.address,
      'bedrooms': instance.bedrooms,
      'bathrooms': instance.bathrooms,
      'price': instance.price,
      'floor_area': instance.floor_area,
      'isFavorite': instance.isFavorite,
      'photos': instance.photos?.map((e) => e?.toJson())?.toList(),
    };

Save$Mutation$User _$Save$Mutation$UserFromJson(Map<String, dynamic> json) {
  return Save$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$Save$Mutation$UserToJson(Save$Mutation$User instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

Save$Mutation _$Save$MutationFromJson(Map<String, dynamic> json) {
  return Save$Mutation()
    ..updateFavoriteList = json['updateFavoriteList'] == null
        ? null
        : Save$Mutation$User.fromJson(
            json['updateFavoriteList'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Save$MutationToJson(Save$Mutation instance) =>
    <String, dynamic>{
      'updateFavoriteList': instance.updateFavoriteList?.toJson(),
    };

SaveArguments _$SaveArgumentsFromJson(Map<String, dynamic> json) {
  return SaveArguments(
    listing_id: json['listing_id'] as String,
  );
}

Map<String, dynamic> _$SaveArgumentsToJson(SaveArguments instance) =>
    <String, dynamic>{
      'listing_id': instance.listing_id,
    };

FavoritedListings$Query$User$Listing
    _$FavoritedListings$Query$User$ListingFromJson(Map<String, dynamic> json) {
  return FavoritedListings$Query$User$Listing()
    ..paginatorInfo = json['paginatorInfo'] == null
        ? null
        : FavoritedListings$Query$ListingPaginator$PaginatorInfo.fromJson(
            json['paginatorInfo'] as Map<String, dynamic>)
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : FavoritedListings$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$FavoritedListings$Query$User$ListingToJson(
        FavoritedListings$Query$User$Listing instance) =>
    <String, dynamic>{
      'paginatorInfo': instance.paginatorInfo?.toJson(),
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

FavoritedListings$Query$User _$FavoritedListings$Query$UserFromJson(
    Map<String, dynamic> json) {
  return FavoritedListings$Query$User()
    ..favorite_listings = (json['favorite_listings'] as List)
        ?.map((e) => e == null
            ? null
            : FavoritedListings$Query$User$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$FavoritedListings$Query$UserToJson(
        FavoritedListings$Query$User instance) =>
    <String, dynamic>{
      'favorite_listings':
          instance.favorite_listings?.map((e) => e?.toJson())?.toList(),
    };

FavoritedListings$Query _$FavoritedListings$QueryFromJson(
    Map<String, dynamic> json) {
  return FavoritedListings$Query()
    ..me = json['me'] == null
        ? null
        : FavoritedListings$Query$User.fromJson(
            json['me'] as Map<String, dynamic>);
}

Map<String, dynamic> _$FavoritedListings$QueryToJson(
        FavoritedListings$Query instance) =>
    <String, dynamic>{
      'me': instance.me?.toJson(),
    };

FavoritedListings$Query$ListingPaginator$PaginatorInfo
    _$FavoritedListings$Query$ListingPaginator$PaginatorInfoFromJson(
        Map<String, dynamic> json) {
  return FavoritedListings$Query$ListingPaginator$PaginatorInfo()
    ..hasMorePages = json['hasMorePages'] as bool;
}

Map<String, dynamic>
    _$FavoritedListings$Query$ListingPaginator$PaginatorInfoToJson(
            FavoritedListings$Query$ListingPaginator$PaginatorInfo instance) =>
        <String, dynamic>{
          'hasMorePages': instance.hasMorePages,
        };

FavoritedListings$Query$ListingPaginator$Listing$Photo
    _$FavoritedListings$Query$ListingPaginator$Listing$PhotoFromJson(
        Map<String, dynamic> json) {
  return FavoritedListings$Query$ListingPaginator$Listing$Photo()
    ..url = json['url'] as String;
}

Map<String, dynamic>
    _$FavoritedListings$Query$ListingPaginator$Listing$PhotoToJson(
            FavoritedListings$Query$ListingPaginator$Listing$Photo instance) =>
        <String, dynamic>{
          'url': instance.url,
        };

FavoritedListings$Query$ListingPaginator$Listing
    _$FavoritedListings$Query$ListingPaginator$ListingFromJson(
        Map<String, dynamic> json) {
  return FavoritedListings$Query$ListingPaginator$Listing()
    ..id = json['id'] as String
    ..title = json['title'] as String
    ..address = json['address'] as String
    ..bedrooms = json['bedrooms'] as int
    ..bathrooms = json['bathrooms'] as int
    ..price = json['price'] as int
    ..floor_area = (json['floor_area'] as num)?.toDouble()
    ..isFavorite = json['isFavorite'] as bool
    ..photos = (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : FavoritedListings$Query$ListingPaginator$Listing$Photo.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$FavoritedListings$Query$ListingPaginator$ListingToJson(
        FavoritedListings$Query$ListingPaginator$Listing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'address': instance.address,
      'bedrooms': instance.bedrooms,
      'bathrooms': instance.bathrooms,
      'price': instance.price,
      'floor_area': instance.floor_area,
      'isFavorite': instance.isFavorite,
      'photos': instance.photos?.map((e) => e?.toJson())?.toList(),
    };

SearchSuggestion$Query$ListingPaginator$Listing
    _$SearchSuggestion$Query$ListingPaginator$ListingFromJson(
        Map<String, dynamic> json) {
  return SearchSuggestion$Query$ListingPaginator$Listing()
    ..title = json['title'] as String
    ..address = json['address'] as String;
}

Map<String, dynamic> _$SearchSuggestion$Query$ListingPaginator$ListingToJson(
        SearchSuggestion$Query$ListingPaginator$Listing instance) =>
    <String, dynamic>{
      'title': instance.title,
      'address': instance.address,
    };

SearchSuggestion$Query$ListingPaginator
    _$SearchSuggestion$Query$ListingPaginatorFromJson(
        Map<String, dynamic> json) {
  return SearchSuggestion$Query$ListingPaginator()
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SearchSuggestion$Query$ListingPaginator$Listing.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SearchSuggestion$Query$ListingPaginatorToJson(
        SearchSuggestion$Query$ListingPaginator instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

SearchSuggestion$Query _$SearchSuggestion$QueryFromJson(
    Map<String, dynamic> json) {
  return SearchSuggestion$Query()
    ..searchListing = json['searchListing'] == null
        ? null
        : SearchSuggestion$Query$ListingPaginator.fromJson(
            json['searchListing'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SearchSuggestion$QueryToJson(
        SearchSuggestion$Query instance) =>
    <String, dynamic>{
      'searchListing': instance.searchListing?.toJson(),
    };

SearchSuggestionArguments _$SearchSuggestionArgumentsFromJson(
    Map<String, dynamic> json) {
  return SearchSuggestionArguments(
    phrase: json['phrase'] as String,
  );
}

Map<String, dynamic> _$SearchSuggestionArgumentsToJson(
        SearchSuggestionArguments instance) =>
    <String, dynamic>{
      'phrase': instance.phrase,
    };

Amenities$Query$AmenityPaginator$Amenity
    _$Amenities$Query$AmenityPaginator$AmenityFromJson(
        Map<String, dynamic> json) {
  return Amenities$Query$AmenityPaginator$Amenity()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$Amenities$Query$AmenityPaginator$AmenityToJson(
        Amenities$Query$AmenityPaginator$Amenity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

Amenities$Query$AmenityPaginator _$Amenities$Query$AmenityPaginatorFromJson(
    Map<String, dynamic> json) {
  return Amenities$Query$AmenityPaginator()
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : Amenities$Query$AmenityPaginator$Amenity.fromJson(
                e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$Amenities$Query$AmenityPaginatorToJson(
        Amenities$Query$AmenityPaginator instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
    };

Amenities$Query _$Amenities$QueryFromJson(Map<String, dynamic> json) {
  return Amenities$Query()
    ..amenities = json['amenities'] == null
        ? null
        : Amenities$Query$AmenityPaginator.fromJson(
            json['amenities'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Amenities$QueryToJson(Amenities$Query instance) =>
    <String, dynamic>{
      'amenities': instance.amenities?.toJson(),
    };
