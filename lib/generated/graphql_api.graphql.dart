// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'graphql_api.graphql.g.dart';

mixin SearchListing$ListItemMixin {
  SearchListing$Query$ListingPaginator$PaginatorInfo paginatorInfo;
  List<SearchListing$Query$ListingPaginator$Listing> data;
}

@JsonSerializable(explicitToJson: true)
class SearchListing$Query$ListingPaginator
    with EquatableMixin, SearchListing$ListItemMixin {
  SearchListing$Query$ListingPaginator();

  factory SearchListing$Query$ListingPaginator.fromJson(
          Map<String, dynamic> json) =>
      _$SearchListing$Query$ListingPaginatorFromJson(json);

  @override
  List<Object> get props => [paginatorInfo, data];
  Map<String, dynamic> toJson() =>
      _$SearchListing$Query$ListingPaginatorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchListing$Query with EquatableMixin {
  SearchListing$Query();

  factory SearchListing$Query.fromJson(Map<String, dynamic> json) =>
      _$SearchListing$QueryFromJson(json);

  SearchListing$Query$ListingPaginator searchListing;

  @override
  List<Object> get props => [searchListing];
  Map<String, dynamic> toJson() => _$SearchListing$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchListing$Query$ListingPaginator$PaginatorInfo with EquatableMixin {
  SearchListing$Query$ListingPaginator$PaginatorInfo();

  factory SearchListing$Query$ListingPaginator$PaginatorInfo.fromJson(
          Map<String, dynamic> json) =>
      _$SearchListing$Query$ListingPaginator$PaginatorInfoFromJson(json);

  bool hasMorePages;

  @override
  List<Object> get props => [hasMorePages];
  Map<String, dynamic> toJson() =>
      _$SearchListing$Query$ListingPaginator$PaginatorInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchListing$Query$ListingPaginator$Listing$Photo with EquatableMixin {
  SearchListing$Query$ListingPaginator$Listing$Photo();

  factory SearchListing$Query$ListingPaginator$Listing$Photo.fromJson(
          Map<String, dynamic> json) =>
      _$SearchListing$Query$ListingPaginator$Listing$PhotoFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() =>
      _$SearchListing$Query$ListingPaginator$Listing$PhotoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchListing$Query$ListingPaginator$Listing with EquatableMixin {
  SearchListing$Query$ListingPaginator$Listing();

  factory SearchListing$Query$ListingPaginator$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$SearchListing$Query$ListingPaginator$ListingFromJson(json);

  String id;

  String title;

  String address;

  int bedrooms;

  int bathrooms;

  int price;

  double floor_area;

  bool isFavorite;

  List<SearchListing$Query$ListingPaginator$Listing$Photo> photos;

  @override
  List<Object> get props => [
        id,
        title,
        address,
        bedrooms,
        bathrooms,
        price,
        floor_area,
        isFavorite,
        photos
      ];
  Map<String, dynamic> toJson() =>
      _$SearchListing$Query$ListingPaginator$ListingToJson(this);
}

enum SearchListing$SortOrder {
  ASC,
  DESC,
  ARTEMIS_UNKNOWN,
}

@JsonSerializable(explicitToJson: true)
class SearchListingArguments extends JsonSerializable with EquatableMixin {
  SearchListingArguments(
      {this.amenities,
      this.bathrooms,
      this.bathrooms_more_than,
      this.bedrooms,
      this.bedrooms_more_than,
      @required this.first,
      this.listing_type_id,
      this.page,
      this.parking_type_id,
      this.pet_friendly,
      this.price_from,
      this.price_to,
      this.search,
      this.available,
      this.couples,
      this.furnished,
      this.orderBy});

  factory SearchListingArguments.fromJson(Map<String, dynamic> json) =>
      _$SearchListingArgumentsFromJson(json);

  final List<int> amenities;

  final int bathrooms;

  final int bathrooms_more_than;

  final int bedrooms;

  final int bedrooms_more_than;

  final int first;

  final int listing_type_id;

  final int page;

  final int parking_type_id;

  final int pet_friendly;

  final int price_from;

  final int price_to;

  final String search;

  final bool available;

  final int couples;

  final bool furnished;

  final SearchListing$SortOrder orderBy;

  @override
  List<Object> get props => [
        amenities,
        bathrooms,
        bathrooms_more_than,
        bedrooms,
        bedrooms_more_than,
        first,
        listing_type_id,
        page,
        parking_type_id,
        pet_friendly,
        price_from,
        price_to,
        search,
        available,
        couples,
        furnished,
        orderBy
      ];
  Map<String, dynamic> toJson() => _$SearchListingArgumentsToJson(this);
}

class SearchListingQuery
    extends GraphQLQuery<SearchListing$Query, SearchListingArguments> {
  SearchListingQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'SearchListing'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'amenities')),
              type: ListTypeNode(
                  type: NamedTypeNode(
                      name: NameNode(value: 'Int'), isNonNull: true),
                  isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'bathrooms')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable:
                  VariableNode(name: NameNode(value: 'bathrooms_more_than')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'bedrooms')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable:
                  VariableNode(name: NameNode(value: 'bedrooms_more_than')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'first')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'listing_type_id')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'page')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'parking_type_id')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'pet_friendly')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'price_from')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'price_to')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'search')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'available')),
              type: NamedTypeNode(
                  name: NameNode(value: 'Boolean'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'couples')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'furnished')),
              type: NamedTypeNode(
                  name: NameNode(value: 'Boolean'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'orderBy')),
              type: NamedTypeNode(
                  name: NameNode(value: 'SortOrder'), isNonNull: false),
              defaultValue: DefaultValueNode(
                  value: EnumValueNode(name: NameNode(value: 'ASC'))),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'searchListing'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'amenities'),
                    value: VariableNode(name: NameNode(value: 'amenities'))),
                ArgumentNode(
                    name: NameNode(value: 'bathrooms'),
                    value: VariableNode(name: NameNode(value: 'bathrooms'))),
                ArgumentNode(
                    name: NameNode(value: 'bathrooms_more_than'),
                    value: VariableNode(
                        name: NameNode(value: 'bathrooms_more_than'))),
                ArgumentNode(
                    name: NameNode(value: 'bedrooms'),
                    value: VariableNode(name: NameNode(value: 'bedrooms'))),
                ArgumentNode(
                    name: NameNode(value: 'bedrooms_more_than'),
                    value: VariableNode(
                        name: NameNode(value: 'bedrooms_more_than'))),
                ArgumentNode(
                    name: NameNode(value: 'first'),
                    value: VariableNode(name: NameNode(value: 'first'))),
                ArgumentNode(
                    name: NameNode(value: 'listing_type_id'),
                    value:
                        VariableNode(name: NameNode(value: 'listing_type_id'))),
                ArgumentNode(
                    name: NameNode(value: 'page'),
                    value: VariableNode(name: NameNode(value: 'page'))),
                ArgumentNode(
                    name: NameNode(value: 'parking_type_id'),
                    value:
                        VariableNode(name: NameNode(value: 'parking_type_id'))),
                ArgumentNode(
                    name: NameNode(value: 'pet_friendly'),
                    value: VariableNode(name: NameNode(value: 'pet_friendly'))),
                ArgumentNode(
                    name: NameNode(value: 'price_from'),
                    value: VariableNode(name: NameNode(value: 'price_from'))),
                ArgumentNode(
                    name: NameNode(value: 'price_to'),
                    value: VariableNode(name: NameNode(value: 'price_to'))),
                ArgumentNode(
                    name: NameNode(value: 'search'),
                    value: VariableNode(name: NameNode(value: 'search'))),
                ArgumentNode(
                    name: NameNode(value: 'available'),
                    value: VariableNode(name: NameNode(value: 'available'))),
                ArgumentNode(
                    name: NameNode(value: 'couples'),
                    value: VariableNode(name: NameNode(value: 'couples'))),
                ArgumentNode(
                    name: NameNode(value: 'furnished'),
                    value: VariableNode(name: NameNode(value: 'furnished'))),
                ArgumentNode(
                    name: NameNode(value: 'orderBy'),
                    value: ListValueNode(values: [
                      ObjectValueNode(fields: [
                        ObjectFieldNode(
                            name: NameNode(value: 'field'),
                            value:
                                EnumValueNode(name: NameNode(value: 'PRICE'))),
                        ObjectFieldNode(
                            name: NameNode(value: 'order'),
                            value:
                                VariableNode(name: NameNode(value: 'orderBy')))
                      ])
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FragmentSpreadNode(
                    name: NameNode(value: 'listItem'), directives: [])
              ]))
        ])),
    FragmentDefinitionNode(
        name: NameNode(value: 'listItem'),
        typeCondition: TypeConditionNode(
            on: NamedTypeNode(
                name: NameNode(value: 'ListingPaginator'), isNonNull: false)),
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'paginatorInfo'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'hasMorePages'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ])),
          FieldNode(
              name: NameNode(value: 'data'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'title'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bedrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bathrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'price'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'floor_area'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isFavorite'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photos'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'url'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'SearchListing';

  @override
  final SearchListingArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  SearchListing$Query parse(Map<String, dynamic> json) =>
      SearchListing$Query.fromJson(json);
}

mixin MessagedListings$ListItemMixin {
  MessagedListings$Query$ListingPaginator$PaginatorInfo paginatorInfo;
  List<MessagedListings$Query$ListingPaginator$Listing> data;
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query$User$Listing
    with EquatableMixin, MessagedListings$ListItemMixin {
  MessagedListings$Query$User$Listing();

  factory MessagedListings$Query$User$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$MessagedListings$Query$User$ListingFromJson(json);

  @override
  List<Object> get props => [paginatorInfo, data];
  Map<String, dynamic> toJson() =>
      _$MessagedListings$Query$User$ListingToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query$User with EquatableMixin {
  MessagedListings$Query$User();

  factory MessagedListings$Query$User.fromJson(Map<String, dynamic> json) =>
      _$MessagedListings$Query$UserFromJson(json);

  List<MessagedListings$Query$User$Listing> messaged_listings;

  @override
  List<Object> get props => [messaged_listings];
  Map<String, dynamic> toJson() => _$MessagedListings$Query$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query with EquatableMixin {
  MessagedListings$Query();

  factory MessagedListings$Query.fromJson(Map<String, dynamic> json) =>
      _$MessagedListings$QueryFromJson(json);

  MessagedListings$Query$User me;

  @override
  List<Object> get props => [me];
  Map<String, dynamic> toJson() => _$MessagedListings$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query$ListingPaginator$PaginatorInfo
    with EquatableMixin {
  MessagedListings$Query$ListingPaginator$PaginatorInfo();

  factory MessagedListings$Query$ListingPaginator$PaginatorInfo.fromJson(
          Map<String, dynamic> json) =>
      _$MessagedListings$Query$ListingPaginator$PaginatorInfoFromJson(json);

  bool hasMorePages;

  @override
  List<Object> get props => [hasMorePages];
  Map<String, dynamic> toJson() =>
      _$MessagedListings$Query$ListingPaginator$PaginatorInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query$ListingPaginator$Listing$Photo
    with EquatableMixin {
  MessagedListings$Query$ListingPaginator$Listing$Photo();

  factory MessagedListings$Query$ListingPaginator$Listing$Photo.fromJson(
          Map<String, dynamic> json) =>
      _$MessagedListings$Query$ListingPaginator$Listing$PhotoFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() =>
      _$MessagedListings$Query$ListingPaginator$Listing$PhotoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MessagedListings$Query$ListingPaginator$Listing with EquatableMixin {
  MessagedListings$Query$ListingPaginator$Listing();

  factory MessagedListings$Query$ListingPaginator$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$MessagedListings$Query$ListingPaginator$ListingFromJson(json);

  String id;

  String title;

  String address;

  int bedrooms;

  int bathrooms;

  int price;

  double floor_area;

  bool isFavorite;

  List<MessagedListings$Query$ListingPaginator$Listing$Photo> photos;

  @override
  List<Object> get props => [
        id,
        title,
        address,
        bedrooms,
        bathrooms,
        price,
        floor_area,
        isFavorite,
        photos
      ];
  Map<String, dynamic> toJson() =>
      _$MessagedListings$Query$ListingPaginator$ListingToJson(this);
}

class MessagedListingsQuery
    extends GraphQLQuery<MessagedListings$Query, JsonSerializable> {
  MessagedListingsQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'MessagedListings'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'me'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'messaged_listings'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FragmentSpreadNode(
                          name: NameNode(value: 'listItem'), directives: [])
                    ]))
              ]))
        ])),
    FragmentDefinitionNode(
        name: NameNode(value: 'listItem'),
        typeCondition: TypeConditionNode(
            on: NamedTypeNode(
                name: NameNode(value: 'ListingPaginator'), isNonNull: false)),
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'paginatorInfo'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'hasMorePages'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ])),
          FieldNode(
              name: NameNode(value: 'data'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'title'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bedrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bathrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'price'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'floor_area'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isFavorite'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photos'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'url'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'MessagedListings';

  @override
  List<Object> get props => [document, operationName];
  @override
  MessagedListings$Query parse(Map<String, dynamic> json) =>
      MessagedListings$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class ForgotPassword$Mutation$ForgotPasswordResponse with EquatableMixin {
  ForgotPassword$Mutation$ForgotPasswordResponse();

  factory ForgotPassword$Mutation$ForgotPasswordResponse.fromJson(
          Map<String, dynamic> json) =>
      _$ForgotPassword$Mutation$ForgotPasswordResponseFromJson(json);

  String status;

  String message;

  @override
  List<Object> get props => [status, message];
  Map<String, dynamic> toJson() =>
      _$ForgotPassword$Mutation$ForgotPasswordResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ForgotPassword$Mutation with EquatableMixin {
  ForgotPassword$Mutation();

  factory ForgotPassword$Mutation.fromJson(Map<String, dynamic> json) =>
      _$ForgotPassword$MutationFromJson(json);

  ForgotPassword$Mutation$ForgotPasswordResponse forgotPassword;

  @override
  List<Object> get props => [forgotPassword];
  Map<String, dynamic> toJson() => _$ForgotPassword$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ForgotPasswordArguments extends JsonSerializable with EquatableMixin {
  ForgotPasswordArguments({@required this.email});

  factory ForgotPasswordArguments.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordArgumentsFromJson(json);

  final String email;

  @override
  List<Object> get props => [email];
  Map<String, dynamic> toJson() => _$ForgotPasswordArgumentsToJson(this);
}

class ForgotPasswordMutation
    extends GraphQLQuery<ForgotPassword$Mutation, ForgotPasswordArguments> {
  ForgotPasswordMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'ForgotPassword'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'email')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'forgotPassword'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'email'),
                          value: VariableNode(name: NameNode(value: 'email')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'status'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'message'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'ForgotPassword';

  @override
  final ForgotPasswordArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  ForgotPassword$Mutation parse(Map<String, dynamic> json) =>
      ForgotPassword$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Unsave$Mutation$User with EquatableMixin {
  Unsave$Mutation$User();

  factory Unsave$Mutation$User.fromJson(Map<String, dynamic> json) =>
      _$Unsave$Mutation$UserFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$Unsave$Mutation$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Unsave$Mutation with EquatableMixin {
  Unsave$Mutation();

  factory Unsave$Mutation.fromJson(Map<String, dynamic> json) =>
      _$Unsave$MutationFromJson(json);

  Unsave$Mutation$User updateFavoriteList;

  @override
  List<Object> get props => [updateFavoriteList];
  Map<String, dynamic> toJson() => _$Unsave$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UnsaveArguments extends JsonSerializable with EquatableMixin {
  UnsaveArguments({@required this.listing_id});

  factory UnsaveArguments.fromJson(Map<String, dynamic> json) =>
      _$UnsaveArgumentsFromJson(json);

  final String listing_id;

  @override
  List<Object> get props => [listing_id];
  Map<String, dynamic> toJson() => _$UnsaveArgumentsToJson(this);
}

class UnsaveMutation extends GraphQLQuery<Unsave$Mutation, UnsaveArguments> {
  UnsaveMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'Unsave'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'listing_id')),
              type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateFavoriteList'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'favorite_listings'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'disconnect'),
                          value: ListValueNode(values: [
                            VariableNode(name: NameNode(value: 'listing_id'))
                          ]))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Unsave';

  @override
  final UnsaveArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Unsave$Mutation parse(Map<String, dynamic> json) =>
      Unsave$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Login$Mutation$AuthPayload$User with EquatableMixin {
  Login$Mutation$AuthPayload$User();

  factory Login$Mutation$AuthPayload$User.fromJson(Map<String, dynamic> json) =>
      _$Login$Mutation$AuthPayload$UserFromJson(json);

  String id;

  String first_name;

  String last_name;

  String email;

  String mobile_phone;

  String avatar;

  @override
  List<Object> get props =>
      [id, first_name, last_name, email, mobile_phone, avatar];
  Map<String, dynamic> toJson() =>
      _$Login$Mutation$AuthPayload$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Mutation$AuthPayload with EquatableMixin {
  Login$Mutation$AuthPayload();

  factory Login$Mutation$AuthPayload.fromJson(Map<String, dynamic> json) =>
      _$Login$Mutation$AuthPayloadFromJson(json);

  String access_token;

  Login$Mutation$AuthPayload$User user;

  @override
  List<Object> get props => [access_token, user];
  Map<String, dynamic> toJson() => _$Login$Mutation$AuthPayloadToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Mutation with EquatableMixin {
  Login$Mutation();

  factory Login$Mutation.fromJson(Map<String, dynamic> json) =>
      _$Login$MutationFromJson(json);

  Login$Mutation$AuthPayload login;

  @override
  List<Object> get props => [login];
  Map<String, dynamic> toJson() => _$Login$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginArguments extends JsonSerializable with EquatableMixin {
  LoginArguments({@required this.username, @required this.password});

  factory LoginArguments.fromJson(Map<String, dynamic> json) =>
      _$LoginArgumentsFromJson(json);

  final String username;

  final String password;

  @override
  List<Object> get props => [username, password];
  Map<String, dynamic> toJson() => _$LoginArgumentsToJson(this);
}

class LoginMutation extends GraphQLQuery<Login$Mutation, LoginArguments> {
  LoginMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'Login'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'username')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'password')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'login'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'username'),
                          value:
                              VariableNode(name: NameNode(value: 'username'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'password'),
                          value:
                              VariableNode(name: NameNode(value: 'password')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'access_token'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'user'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'first_name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'last_name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'email'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'mobile_phone'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'avatar'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Login';

  @override
  final LoginArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Login$Mutation parse(Map<String, dynamic> json) =>
      Login$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Register$Mutation$RegisterResponse with EquatableMixin {
  Register$Mutation$RegisterResponse();

  factory Register$Mutation$RegisterResponse.fromJson(
          Map<String, dynamic> json) =>
      _$Register$Mutation$RegisterResponseFromJson(json);

  @JsonKey(
      unknownEnumValue:
          Register$Mutation$RegisterResponse$RegisterStatuses.ARTEMIS_UNKNOWN)
  Register$Mutation$RegisterResponse$RegisterStatuses status;

  @override
  List<Object> get props => [status];
  Map<String, dynamic> toJson() =>
      _$Register$Mutation$RegisterResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Register$Mutation with EquatableMixin {
  Register$Mutation();

  factory Register$Mutation.fromJson(Map<String, dynamic> json) =>
      _$Register$MutationFromJson(json);

  Register$Mutation$RegisterResponse register;

  @override
  List<Object> get props => [register];
  Map<String, dynamic> toJson() => _$Register$MutationToJson(this);
}

enum Register$Mutation$RegisterResponse$RegisterStatuses {
  MUST_VERIFY_EMAIL,
  SUCCESS,
  ARTEMIS_UNKNOWN,
}

@JsonSerializable(explicitToJson: true)
class RegisterArguments extends JsonSerializable with EquatableMixin {
  RegisterArguments(
      {@required this.first_name,
      @required this.last_name,
      @required this.email,
      @required this.password,
      @required this.password_confirmation});

  factory RegisterArguments.fromJson(Map<String, dynamic> json) =>
      _$RegisterArgumentsFromJson(json);

  final String first_name;

  final String last_name;

  final String email;

  final String password;

  final String password_confirmation;

  @override
  List<Object> get props =>
      [first_name, last_name, email, password, password_confirmation];
  Map<String, dynamic> toJson() => _$RegisterArgumentsToJson(this);
}

class RegisterMutation
    extends GraphQLQuery<Register$Mutation, RegisterArguments> {
  RegisterMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'Register'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'first_name')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'last_name')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'email')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'password')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable:
                  VariableNode(name: NameNode(value: 'password_confirmation')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'register'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'first_name'),
                          value: VariableNode(
                              name: NameNode(value: 'first_name'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'last_name'),
                          value:
                              VariableNode(name: NameNode(value: 'last_name'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'email'),
                          value: VariableNode(name: NameNode(value: 'email'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'password'),
                          value:
                              VariableNode(name: NameNode(value: 'password'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'password_confirmation'),
                          value: VariableNode(
                              name: NameNode(value: 'password_confirmation')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'status'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Register';

  @override
  final RegisterArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Register$Mutation parse(Map<String, dynamic> json) =>
      Register$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Property$Query$Listing$Amenity with EquatableMixin {
  Property$Query$Listing$Amenity();

  factory Property$Query$Listing$Amenity.fromJson(Map<String, dynamic> json) =>
      _$Property$Query$Listing$AmenityFromJson(json);

  String name;

  @override
  List<Object> get props => [name];
  Map<String, dynamic> toJson() => _$Property$Query$Listing$AmenityToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Property$Query$Listing$ListingDescription with EquatableMixin {
  Property$Query$Listing$ListingDescription();

  factory Property$Query$Listing$ListingDescription.fromJson(
          Map<String, dynamic> json) =>
      _$Property$Query$Listing$ListingDescriptionFromJson(json);

  String language;

  String description;

  @override
  List<Object> get props => [language, description];
  Map<String, dynamic> toJson() =>
      _$Property$Query$Listing$ListingDescriptionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Property$Query$Listing$Photo with EquatableMixin {
  Property$Query$Listing$Photo();

  factory Property$Query$Listing$Photo.fromJson(Map<String, dynamic> json) =>
      _$Property$Query$Listing$PhotoFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() => _$Property$Query$Listing$PhotoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Property$Query$Listing with EquatableMixin {
  Property$Query$Listing();

  factory Property$Query$Listing.fromJson(Map<String, dynamic> json) =>
      _$Property$Query$ListingFromJson(json);

  String id;

  String title;

  String address;

  bool pet_friendly;

  bool isFavorite;

  int bedrooms;

  int bathrooms;

  int price;

  String reference;

  double latitude;

  double longitude;

  double floor_area;

  bool isMessaged;

  List<Property$Query$Listing$Amenity> amenities;

  List<Property$Query$Listing$ListingDescription> descriptions;

  List<Property$Query$Listing$Photo> photos;

  @override
  List<Object> get props => [
        id,
        title,
        address,
        pet_friendly,
        isFavorite,
        bedrooms,
        bathrooms,
        price,
        reference,
        latitude,
        longitude,
        floor_area,
        isMessaged,
        amenities,
        descriptions,
        photos
      ];
  Map<String, dynamic> toJson() => _$Property$Query$ListingToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Property$Query with EquatableMixin {
  Property$Query();

  factory Property$Query.fromJson(Map<String, dynamic> json) =>
      _$Property$QueryFromJson(json);

  Property$Query$Listing listing;

  @override
  List<Object> get props => [listing];
  Map<String, dynamic> toJson() => _$Property$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class PropertyArguments extends JsonSerializable with EquatableMixin {
  PropertyArguments({@required this.id});

  factory PropertyArguments.fromJson(Map<String, dynamic> json) =>
      _$PropertyArgumentsFromJson(json);

  final String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$PropertyArgumentsToJson(this);
}

class PropertyQuery extends GraphQLQuery<Property$Query, PropertyArguments> {
  PropertyQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Property'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'id')),
              type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'listing'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'id')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'title'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'pet_friendly'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isFavorite'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bedrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bathrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'price'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'reference'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'latitude'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'longitude'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'floor_area'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isMessaged'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'amenities'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'descriptions'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'language'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'description'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'photos'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'url'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Property';

  @override
  final PropertyArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Property$Query parse(Map<String, dynamic> json) =>
      Property$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class MakeEnquiry$Mutation$Lead with EquatableMixin {
  MakeEnquiry$Mutation$Lead();

  factory MakeEnquiry$Mutation$Lead.fromJson(Map<String, dynamic> json) =>
      _$MakeEnquiry$Mutation$LeadFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$MakeEnquiry$Mutation$LeadToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MakeEnquiry$Mutation with EquatableMixin {
  MakeEnquiry$Mutation();

  factory MakeEnquiry$Mutation.fromJson(Map<String, dynamic> json) =>
      _$MakeEnquiry$MutationFromJson(json);

  MakeEnquiry$Mutation$Lead createLead;

  @override
  List<Object> get props => [createLead];
  Map<String, dynamic> toJson() => _$MakeEnquiry$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MakeEnquiryArguments extends JsonSerializable with EquatableMixin {
  MakeEnquiryArguments(
      {@required this.listing_id,
      @required this.first_name,
      @required this.last_name,
      @required this.email,
      @required this.phone,
      @required this.message});

  factory MakeEnquiryArguments.fromJson(Map<String, dynamic> json) =>
      _$MakeEnquiryArgumentsFromJson(json);

  final int listing_id;

  final String first_name;

  final String last_name;

  final String email;

  final String phone;

  final String message;

  @override
  List<Object> get props =>
      [listing_id, first_name, last_name, email, phone, message];
  Map<String, dynamic> toJson() => _$MakeEnquiryArgumentsToJson(this);
}

class MakeEnquiryMutation
    extends GraphQLQuery<MakeEnquiry$Mutation, MakeEnquiryArguments> {
  MakeEnquiryMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'MakeEnquiry'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'listing_id')),
              type:
                  NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'first_name')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'last_name')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'email')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'phone')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'message')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'createLead'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'listing_id'),
                          value: VariableNode(
                              name: NameNode(value: 'listing_id'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'first_name'),
                          value: VariableNode(
                              name: NameNode(value: 'first_name'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'last_name'),
                          value:
                              VariableNode(name: NameNode(value: 'last_name'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'email'),
                          value: VariableNode(name: NameNode(value: 'email'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'phone'),
                          value: VariableNode(name: NameNode(value: 'phone'))),
                      ObjectFieldNode(
                          name: NameNode(value: 'message'),
                          value: VariableNode(name: NameNode(value: 'message')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'MakeEnquiry';

  @override
  final MakeEnquiryArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  MakeEnquiry$Mutation parse(Map<String, dynamic> json) =>
      MakeEnquiry$Mutation.fromJson(json);
}

mixin Home$ListItemMixin {
  Home$Query$ListingPaginator$PaginatorInfo paginatorInfo;
  List<Home$Query$ListingPaginator$Listing> data;
}

@JsonSerializable(explicitToJson: true)
class Home$Query$AvailableNow with EquatableMixin, Home$ListItemMixin {
  Home$Query$AvailableNow();

  factory Home$Query$AvailableNow.fromJson(Map<String, dynamic> json) =>
      _$Home$Query$AvailableNowFromJson(json);

  @override
  List<Object> get props => [paginatorInfo, data];
  Map<String, dynamic> toJson() => _$Home$Query$AvailableNowToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Home$Query$PetFriendly with EquatableMixin, Home$ListItemMixin {
  Home$Query$PetFriendly();

  factory Home$Query$PetFriendly.fromJson(Map<String, dynamic> json) =>
      _$Home$Query$PetFriendlyFromJson(json);

  @override
  List<Object> get props => [paginatorInfo, data];
  Map<String, dynamic> toJson() => _$Home$Query$PetFriendlyToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Home$Query with EquatableMixin {
  Home$Query();

  factory Home$Query.fromJson(Map<String, dynamic> json) =>
      _$Home$QueryFromJson(json);

  Home$Query$AvailableNow availableNow;

  Home$Query$PetFriendly petFriendly;

  @override
  List<Object> get props => [availableNow, petFriendly];
  Map<String, dynamic> toJson() => _$Home$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Home$Query$ListingPaginator$PaginatorInfo with EquatableMixin {
  Home$Query$ListingPaginator$PaginatorInfo();

  factory Home$Query$ListingPaginator$PaginatorInfo.fromJson(
          Map<String, dynamic> json) =>
      _$Home$Query$ListingPaginator$PaginatorInfoFromJson(json);

  bool hasMorePages;

  @override
  List<Object> get props => [hasMorePages];
  Map<String, dynamic> toJson() =>
      _$Home$Query$ListingPaginator$PaginatorInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Home$Query$ListingPaginator$Listing$Photo with EquatableMixin {
  Home$Query$ListingPaginator$Listing$Photo();

  factory Home$Query$ListingPaginator$Listing$Photo.fromJson(
          Map<String, dynamic> json) =>
      _$Home$Query$ListingPaginator$Listing$PhotoFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() =>
      _$Home$Query$ListingPaginator$Listing$PhotoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Home$Query$ListingPaginator$Listing with EquatableMixin {
  Home$Query$ListingPaginator$Listing();

  factory Home$Query$ListingPaginator$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$Home$Query$ListingPaginator$ListingFromJson(json);

  String id;

  String title;

  String address;

  int bedrooms;

  int bathrooms;

  int price;

  double floor_area;

  bool isFavorite;

  List<Home$Query$ListingPaginator$Listing$Photo> photos;

  @override
  List<Object> get props => [
        id,
        title,
        address,
        bedrooms,
        bathrooms,
        price,
        floor_area,
        isFavorite,
        photos
      ];
  Map<String, dynamic> toJson() =>
      _$Home$Query$ListingPaginator$ListingToJson(this);
}

class HomeQuery extends GraphQLQuery<Home$Query, JsonSerializable> {
  HomeQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Home'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'searchListing'),
              alias: NameNode(value: 'availableNow'),
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'first'),
                    value: IntValueNode(value: '6')),
                ArgumentNode(
                    name: NameNode(value: 'page'),
                    value: IntValueNode(value: '1')),
                ArgumentNode(
                    name: NameNode(value: 'available'),
                    value: BooleanValueNode(value: true)),
                ArgumentNode(
                    name: NameNode(value: 'search'),
                    value: StringValueNode(value: '', isBlock: false))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FragmentSpreadNode(
                    name: NameNode(value: 'listItem'), directives: [])
              ])),
          FieldNode(
              name: NameNode(value: 'searchListing'),
              alias: NameNode(value: 'petFriendly'),
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'first'),
                    value: IntValueNode(value: '6')),
                ArgumentNode(
                    name: NameNode(value: 'page'),
                    value: IntValueNode(value: '1')),
                ArgumentNode(
                    name: NameNode(value: 'pet_friendly'),
                    value: IntValueNode(value: '1')),
                ArgumentNode(
                    name: NameNode(value: 'search'),
                    value: StringValueNode(value: '', isBlock: false))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FragmentSpreadNode(
                    name: NameNode(value: 'listItem'), directives: [])
              ]))
        ])),
    FragmentDefinitionNode(
        name: NameNode(value: 'listItem'),
        typeCondition: TypeConditionNode(
            on: NamedTypeNode(
                name: NameNode(value: 'ListingPaginator'), isNonNull: false)),
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'paginatorInfo'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'hasMorePages'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ])),
          FieldNode(
              name: NameNode(value: 'data'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'title'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bedrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bathrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'price'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'floor_area'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isFavorite'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photos'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'url'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Home';

  @override
  List<Object> get props => [document, operationName];
  @override
  Home$Query parse(Map<String, dynamic> json) => Home$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Save$Mutation$User with EquatableMixin {
  Save$Mutation$User();

  factory Save$Mutation$User.fromJson(Map<String, dynamic> json) =>
      _$Save$Mutation$UserFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$Save$Mutation$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Save$Mutation with EquatableMixin {
  Save$Mutation();

  factory Save$Mutation.fromJson(Map<String, dynamic> json) =>
      _$Save$MutationFromJson(json);

  Save$Mutation$User updateFavoriteList;

  @override
  List<Object> get props => [updateFavoriteList];
  Map<String, dynamic> toJson() => _$Save$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SaveArguments extends JsonSerializable with EquatableMixin {
  SaveArguments({@required this.listing_id});

  factory SaveArguments.fromJson(Map<String, dynamic> json) =>
      _$SaveArgumentsFromJson(json);

  final String listing_id;

  @override
  List<Object> get props => [listing_id];
  Map<String, dynamic> toJson() => _$SaveArgumentsToJson(this);
}

class SaveMutation extends GraphQLQuery<Save$Mutation, SaveArguments> {
  SaveMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'Save'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'listing_id')),
              type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateFavoriteList'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'favorite_listings'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'syncWithoutDetaching'),
                          value: ListValueNode(values: [
                            VariableNode(name: NameNode(value: 'listing_id'))
                          ]))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Save';

  @override
  final SaveArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Save$Mutation parse(Map<String, dynamic> json) =>
      Save$Mutation.fromJson(json);
}

mixin FavoritedListings$ListItemMixin {
  FavoritedListings$Query$ListingPaginator$PaginatorInfo paginatorInfo;
  List<FavoritedListings$Query$ListingPaginator$Listing> data;
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query$User$Listing
    with EquatableMixin, FavoritedListings$ListItemMixin {
  FavoritedListings$Query$User$Listing();

  factory FavoritedListings$Query$User$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$FavoritedListings$Query$User$ListingFromJson(json);

  @override
  List<Object> get props => [paginatorInfo, data];
  Map<String, dynamic> toJson() =>
      _$FavoritedListings$Query$User$ListingToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query$User with EquatableMixin {
  FavoritedListings$Query$User();

  factory FavoritedListings$Query$User.fromJson(Map<String, dynamic> json) =>
      _$FavoritedListings$Query$UserFromJson(json);

  List<FavoritedListings$Query$User$Listing> favorite_listings;

  @override
  List<Object> get props => [favorite_listings];
  Map<String, dynamic> toJson() => _$FavoritedListings$Query$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query with EquatableMixin {
  FavoritedListings$Query();

  factory FavoritedListings$Query.fromJson(Map<String, dynamic> json) =>
      _$FavoritedListings$QueryFromJson(json);

  FavoritedListings$Query$User me;

  @override
  List<Object> get props => [me];
  Map<String, dynamic> toJson() => _$FavoritedListings$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query$ListingPaginator$PaginatorInfo
    with EquatableMixin {
  FavoritedListings$Query$ListingPaginator$PaginatorInfo();

  factory FavoritedListings$Query$ListingPaginator$PaginatorInfo.fromJson(
          Map<String, dynamic> json) =>
      _$FavoritedListings$Query$ListingPaginator$PaginatorInfoFromJson(json);

  bool hasMorePages;

  @override
  List<Object> get props => [hasMorePages];
  Map<String, dynamic> toJson() =>
      _$FavoritedListings$Query$ListingPaginator$PaginatorInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query$ListingPaginator$Listing$Photo
    with EquatableMixin {
  FavoritedListings$Query$ListingPaginator$Listing$Photo();

  factory FavoritedListings$Query$ListingPaginator$Listing$Photo.fromJson(
          Map<String, dynamic> json) =>
      _$FavoritedListings$Query$ListingPaginator$Listing$PhotoFromJson(json);

  String url;

  @override
  List<Object> get props => [url];
  Map<String, dynamic> toJson() =>
      _$FavoritedListings$Query$ListingPaginator$Listing$PhotoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FavoritedListings$Query$ListingPaginator$Listing with EquatableMixin {
  FavoritedListings$Query$ListingPaginator$Listing();

  factory FavoritedListings$Query$ListingPaginator$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$FavoritedListings$Query$ListingPaginator$ListingFromJson(json);

  String id;

  String title;

  String address;

  int bedrooms;

  int bathrooms;

  int price;

  double floor_area;

  bool isFavorite;

  List<FavoritedListings$Query$ListingPaginator$Listing$Photo> photos;

  @override
  List<Object> get props => [
        id,
        title,
        address,
        bedrooms,
        bathrooms,
        price,
        floor_area,
        isFavorite,
        photos
      ];
  Map<String, dynamic> toJson() =>
      _$FavoritedListings$Query$ListingPaginator$ListingToJson(this);
}

class FavoritedListingsQuery
    extends GraphQLQuery<FavoritedListings$Query, JsonSerializable> {
  FavoritedListingsQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'FavoritedListings'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'me'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'favorite_listings'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FragmentSpreadNode(
                          name: NameNode(value: 'listItem'), directives: [])
                    ]))
              ]))
        ])),
    FragmentDefinitionNode(
        name: NameNode(value: 'listItem'),
        typeCondition: TypeConditionNode(
            on: NamedTypeNode(
                name: NameNode(value: 'ListingPaginator'), isNonNull: false)),
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'paginatorInfo'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'hasMorePages'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ])),
          FieldNode(
              name: NameNode(value: 'data'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'title'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bedrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'bathrooms'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'price'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'floor_area'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isFavorite'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'photos'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'url'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'FavoritedListings';

  @override
  List<Object> get props => [document, operationName];
  @override
  FavoritedListings$Query parse(Map<String, dynamic> json) =>
      FavoritedListings$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class SearchSuggestion$Query$ListingPaginator$Listing with EquatableMixin {
  SearchSuggestion$Query$ListingPaginator$Listing();

  factory SearchSuggestion$Query$ListingPaginator$Listing.fromJson(
          Map<String, dynamic> json) =>
      _$SearchSuggestion$Query$ListingPaginator$ListingFromJson(json);

  String title;

  String address;

  @override
  List<Object> get props => [title, address];
  Map<String, dynamic> toJson() =>
      _$SearchSuggestion$Query$ListingPaginator$ListingToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchSuggestion$Query$ListingPaginator with EquatableMixin {
  SearchSuggestion$Query$ListingPaginator();

  factory SearchSuggestion$Query$ListingPaginator.fromJson(
          Map<String, dynamic> json) =>
      _$SearchSuggestion$Query$ListingPaginatorFromJson(json);

  List<SearchSuggestion$Query$ListingPaginator$Listing> data;

  @override
  List<Object> get props => [data];
  Map<String, dynamic> toJson() =>
      _$SearchSuggestion$Query$ListingPaginatorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchSuggestion$Query with EquatableMixin {
  SearchSuggestion$Query();

  factory SearchSuggestion$Query.fromJson(Map<String, dynamic> json) =>
      _$SearchSuggestion$QueryFromJson(json);

  SearchSuggestion$Query$ListingPaginator searchListing;

  @override
  List<Object> get props => [searchListing];
  Map<String, dynamic> toJson() => _$SearchSuggestion$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SearchSuggestionArguments extends JsonSerializable with EquatableMixin {
  SearchSuggestionArguments({@required this.phrase});

  factory SearchSuggestionArguments.fromJson(Map<String, dynamic> json) =>
      _$SearchSuggestionArgumentsFromJson(json);

  final String phrase;

  @override
  List<Object> get props => [phrase];
  Map<String, dynamic> toJson() => _$SearchSuggestionArgumentsToJson(this);
}

class SearchSuggestionQuery
    extends GraphQLQuery<SearchSuggestion$Query, SearchSuggestionArguments> {
  SearchSuggestionQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'SearchSuggestion'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'phrase')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'searchListing'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'search'),
                    value: VariableNode(name: NameNode(value: 'phrase'))),
                ArgumentNode(
                    name: NameNode(value: 'first'),
                    value: IntValueNode(value: '20'))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'data'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'title'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'address'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'SearchSuggestion';

  @override
  final SearchSuggestionArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  SearchSuggestion$Query parse(Map<String, dynamic> json) =>
      SearchSuggestion$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Amenities$Query$AmenityPaginator$Amenity with EquatableMixin {
  Amenities$Query$AmenityPaginator$Amenity();

  factory Amenities$Query$AmenityPaginator$Amenity.fromJson(
          Map<String, dynamic> json) =>
      _$Amenities$Query$AmenityPaginator$AmenityFromJson(json);

  String id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() =>
      _$Amenities$Query$AmenityPaginator$AmenityToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Amenities$Query$AmenityPaginator with EquatableMixin {
  Amenities$Query$AmenityPaginator();

  factory Amenities$Query$AmenityPaginator.fromJson(
          Map<String, dynamic> json) =>
      _$Amenities$Query$AmenityPaginatorFromJson(json);

  List<Amenities$Query$AmenityPaginator$Amenity> data;

  @override
  List<Object> get props => [data];
  Map<String, dynamic> toJson() =>
      _$Amenities$Query$AmenityPaginatorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Amenities$Query with EquatableMixin {
  Amenities$Query();

  factory Amenities$Query.fromJson(Map<String, dynamic> json) =>
      _$Amenities$QueryFromJson(json);

  Amenities$Query$AmenityPaginator amenities;

  @override
  List<Object> get props => [amenities];
  Map<String, dynamic> toJson() => _$Amenities$QueryToJson(this);
}

class AmenitiesQuery extends GraphQLQuery<Amenities$Query, JsonSerializable> {
  AmenitiesQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'Amenities'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'amenities'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'first'),
                    value: IntValueNode(value: '10000'))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'data'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'Amenities';

  @override
  List<Object> get props => [document, operationName];
  @override
  Amenities$Query parse(Map<String, dynamic> json) =>
      Amenities$Query.fromJson(json);
}
