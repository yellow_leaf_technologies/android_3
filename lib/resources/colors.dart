import 'package:flutter/material.dart';

class AppColors {
  static const Color THEME_COLOR = Color(0xFF23b0bc);
  static const Color GRAY_COLOR = Color(0xFFb8b8b8);
  static const Color BLACK_COLOR = Color(0xFF092e28);
  static const Color DARK_GRAY_COLOR = Color(0xFF6c827e);
  static const Color INPUT_GRAY_COLOR = Color(0xFFb8b8b8);
  static const Color WHITE_COLOR = Color(0xFFffffff);
  static const Color GRAY_TEXT_COLOR = Color(0xFF939393);
  static const Color BLUE_WHITE = Color(0xFFf4f9f9);
  static const Color GREY_WHITE = Color(0xFFe1e3e3);
  static const Color GREY_LINE = Color(0xFFeeefef);
  static const Color BROWNISH_GREY = Color(0xFF646464);

  static const MaterialColor MATERIAL_THEME_COLOR = const MaterialColor(
    0xFF23b0bc,
    const <int, Color>{
      50: const Color(0xFF23b0bc),
      100: const Color(0xFF23b0bc),
      200: const Color(0xFF23b0bc),
      300: const Color(0xFF23b0bc),
      400: const Color(0xFF23b0bc),
      500: const Color(0xFF23b0bc),
      600: const Color(0xFF23b0bc),
      700: const Color(0xFF23b0bc),
      800: const Color(0xFF23b0bc),
      900: const Color(0xFF23b0bc),
    },
  );
}
