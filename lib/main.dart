import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:moovle/presenter/widgets/entry_point.dart';
import 'package:moovle/resources/constants.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth.dart';

Future<void> main() async {
  await Hive.initFlutter();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Auth auth;

  @override
  void initState() {
    super.initState();
    Auth.instance.then((instance) {
      setState(() {
        auth = instance;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(
      uri: serverUri,
    );

    final AuthLink authLink = AuthLink(
      getToken: () async {
        if (auth.isAuthenticated) {
          if (DateTime.now().isAfter(auth.expired)) {
            await auth.reset();
            return null;
          }
          return 'Bearer ${auth.token}';
        } else {
          return null;
        }
      },
    );
    final Link link = authLink.concat(httpLink);

    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
          cache: InMemoryCache(),
          link: link,
          defaultPolicies: DefaultPolicies(
              watchQuery: Policies.safe(
                FetchPolicy.noCache,
                ErrorPolicy.none,
              ),
              query: Policies(
                fetch: FetchPolicy.noCache,
                error: ErrorPolicy.none,
              ))),
    );

    return GraphQLProvider(
        client: client,
        child: CupertinoApp(
            title: 'Moovle',
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: supportedLocales,
            theme: CupertinoThemeData(
                textTheme: CupertinoTextThemeData(
                  textStyle: TextStyle(
                      fontFamily: 'Circular',
                      color: Color(0xff000000),
                      fontSize: 16),
                ),
                scaffoldBackgroundColor: Color(0xffffffff)),
            home: auth == null
                ? CupertinoPageScaffold(
                    child: Center(
                      child: CupertinoActivityIndicator(),
                    ),
                  )
                : EntryPoint(auth: auth)));
  }
}
