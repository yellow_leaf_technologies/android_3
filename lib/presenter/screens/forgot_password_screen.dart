import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/moovle_button.dart';
import 'package:moovle/presenter/widgets/moovle_text_field.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
          child: Mutation(
        options: MutationOptions(
            documentNode: ForgotPasswordMutation().document,
            onCompleted: (dynamic resultData) async {
              if (resultData != null) {
                var result =
                    ForgotPasswordMutation().parse(resultData).forgotPassword;
                showDialog<void>(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    return CupertinoAlertDialog(
                      title: Text(
                          AppLocalizations.of(context).translation.success),
                      content: Text(result.message),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child:
                              Text(AppLocalizations.of(context).translation.ok),
                          onPressed: () {
                            Navigator.of(context).pop();
                            if (result.status == 'EMAIL_SENT') {
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                          },
                        ),
                      ],
                    );
                  },
                );
              }
            },
            onError: (exception) {
              showDialog<void>(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: Text(AppLocalizations.of(context)
                        .translation
                        .an_error_has_occurred),
                    content: Text(AppLocalizations.of(context)
                        .translation
                        .unexpected_error_occurred),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child:
                            Text(AppLocalizations.of(context).translation.ok),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            }),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) {
          return ForgotPasswordLayout(
              runMutation: runMutation, loading: result.loading);
        },
      )),
    ));
  }
}

class ForgotPasswordLayout extends StatefulWidget {
  final bool loading;
  final RunMutation runMutation;

  const ForgotPasswordLayout({Key key, this.loading, this.runMutation})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _ForgotPasswordScreenState();
  }
}

class _ForgotPasswordScreenState extends State<ForgotPasswordLayout> {
  TextEditingController _emailController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController(text: '');
  }

  @override
  Widget build(BuildContext context) {
    return Material(child: LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 9),
                    alignment: Alignment.centerLeft,
                    child: CloseButton(),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translation
                                  .forgot_password,
                              style: TextStyle(
                                  color: AppColors.BLACK_COLOR,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 35),
                              alignment: Alignment.centerLeft,
                              child: MoovleTextField(
                                AppLocalizations.of(context)
                                    .translation
                                    .email_address,
                                AppLocalizations.of(context)
                                    .translation
                                    .email_placeholder,
                                TextInputType.emailAddress,
                                controller: _emailController,
                                validator: (value) {
                                  if (!EmailValidator.validate(value)) {
                                    return AppLocalizations.of(context)
                                        .translation
                                        .please_enter_valid_email;
                                  }
                                  return null;
                                },
                              )),
                          Container(
                            padding: EdgeInsets.only(top: 26),
                            child: MoovleButton(
                                AppLocalizations.of(context)
                                    .translation
                                    .reset_password, () {
                              if (_formKey.currentState.validate()) {
                                widget.runMutation(
                                    {"email": _emailController.text});
                              }
                            }, loading: widget.loading),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}
