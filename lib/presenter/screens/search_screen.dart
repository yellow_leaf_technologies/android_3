import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:moovle/presenter/screens/filter_search_screen.dart';
import 'package:moovle/presenter/screens/search/recent_searches.dart';
import 'package:moovle/presenter/screens/search/search_results.dart';
import 'package:moovle/presenter/screens/search/search_suggestions.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/search_field.dart';
import 'package:moovle/services/user_data.dart';

enum SearchContent { recentSearches, searchSuggestions, searchResults }

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  UserData _userData;
  TextEditingController _controller;
  SearchContent shownContent = SearchContent.recentSearches;
  String oldPhrase = '';
  String deferredPhrase;
  Timer timer;
  FocusNode focusNode;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();

    _controller = TextEditingController();
    UserData.instance.then((instance) {
      setState(() {
        _userData = instance;
      });
    });

    _controller.addListener(() {
      if (_controller.text.isEmpty &&
          shownContent != SearchContent.recentSearches) {
        setState(() {
          shownContent = SearchContent.recentSearches;
        });
      } else if (_controller.text.isNotEmpty &&
          _controller.text != oldPhrase &&
          shownContent != SearchContent.searchSuggestions) {
        setState(() {
          shownContent = SearchContent.searchSuggestions;
        });
      }
      oldPhrase = _controller.text;

      if (timer != null) {
        timer.cancel();
      }
      timer = Timer(Duration(seconds: 1), () {
        if (mounted) {
          setState(() {
            deferredPhrase = _controller.text;
          });
        }
      });
    });
  }

  Widget buildContent() {
    if (_userData == null) {
      return Center(child: CupertinoActivityIndicator());
    }
    switch (shownContent) {
      case SearchContent.searchSuggestions:
        return SearchSuggestions(
          phrase: deferredPhrase,
          showResults: showResults,
        );
      case SearchContent.searchResults:
        return SearchResults(
          filters: _userData.filters,
          phrase: _controller.text,
        );
      default:
        return RecentSearches(
          showResults: showResults,
          userData: _userData,
        );
    }
  }

  void showResults({String value, bool addToHistory = true}) {
    if (value != null) {
      _controller.text = value;
    }
    if (addToHistory) {
      _userData.addToHistory(_controller.text);
    }
    setState(() {
      shownContent = SearchContent.searchResults;
    });
    focusNode.unfocus();
  }

  @override
  void dispose() {
    _controller.dispose();
    focusNode.unfocus();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CloseButton(),
                CupertinoButton(
                  child: Text('Filters'),
                  onPressed: () {
                    if (_userData != null) {
                      Navigator.of(context, rootNavigator: true)
                          .push(CupertinoPageRoute(builder: (context) {
                        return FilterSearchScreen(filters: _userData.filters);
                      })).then((_filters) {
                        setState(() {
                          _userData.changeFilters(_filters);
                        });
                      });
                    }
                  },
                )
              ],
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: SearchField(
                focusNode: focusNode,
                focus: true,
                controller: _controller,
                onEditingComplete: () {
                  showResults();
                },
              ),
            ),
            Expanded(
              child: buildContent(),
            ),
          ],
        ),
      ),
    ));
  }
}

typedef ShowResults = void Function({String value, bool addToHistory});
