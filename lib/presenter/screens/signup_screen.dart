import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/presenter/screens/login_screen.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/moovle_button.dart';
import 'package:moovle/presenter/widgets/moovle_text_field.dart';
import 'package:moovle/presenter/widgets/password_text_field.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
          child: Mutation(
        options: MutationOptions(
            documentNode: RegisterMutation().document,
            onCompleted: (dynamic resultData) async {
              if (resultData != null) {
                if (RegisterMutation().parse(resultData).register.status ==
                    Register$Mutation$RegisterResponse$RegisterStatuses
                        .SUCCESS) {
                  showDialog<void>(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return CupertinoAlertDialog(
                        title: Text(
                            AppLocalizations.of(context).translation.success),
                        content: Text(AppLocalizations.of(context)
                            .translation
                            .signed_up_successfully),
                        actions: <Widget>[
                          CupertinoDialogAction(
                            child: Text(
                                AppLocalizations.of(context).translation.ok),
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.of(context, rootNavigator: true)
                                  .push(CupertinoPageRoute(builder: (context) {
                                return LoginScreen();
                              }));
                            },
                          ),
                        ],
                      );
                    },
                  );
                }
              }
            },
            onError: (exception) {
              showDialog<void>(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: Text(AppLocalizations.of(context)
                        .translation
                        .an_error_has_occurred),
                    content: Text(AppLocalizations.of(context)
                        .translation
                        .did_not_sign_up),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child:
                            Text(AppLocalizations.of(context).translation.ok),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            }),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) {
          return SignUpLayout(
              runMutation: runMutation, loading: result.loading);
        },
      )),
    ));
  }
}

class SignUpLayout extends StatefulWidget {
  final bool loading;
  final RunMutation runMutation;

  const SignUpLayout({Key key, this.loading, this.runMutation})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SignUpLayoutState();
  }
}

class _SignUpLayoutState extends State<SignUpLayout> {
  TextEditingController _emailController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _passController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController(text: '');
    _firstNameController = TextEditingController(text: '');
    _lastNameController = TextEditingController(text: '');
    _passController = TextEditingController(text: '');
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 9),
                    alignment: Alignment.centerLeft,
                    child: CloseButton(),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translation
                                  .sign_up_to_moovle,
                              style: TextStyle(
                                  color: AppColors.BLACK_COLOR,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 35),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .email_address,
                              AppLocalizations.of(context)
                                  .translation
                                  .email_placeholder,
                              TextInputType.emailAddress,
                              controller: _emailController,
                              validator: (value) {
                                if (!EmailValidator.validate(value)) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_enter_valid_email;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .first_name,
                              AppLocalizations.of(context)
                                  .translation
                                  .enter_your_first_name,
                              TextInputType.text,
                              controller: _firstNameController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context).translation.surname,
                              AppLocalizations.of(context)
                                  .translation
                                  .enter_your_surname,
                              TextInputType.text,
                              controller: _lastNameController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: PasswordTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .enter_your_password,
                              controller: _passController,
                              validator: (value) {
                                if (value.isEmpty || value.length < 8) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .password_least_character;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 26),
                            child: MoovleButton(
                                AppLocalizations.of(context)
                                    .translation
                                    .log_in_find_home, () {
                              if (_formKey.currentState.validate()) {
                                widget.runMutation(RegisterArguments(
                                        first_name: _firstNameController.text,
                                        last_name: _lastNameController.text,
                                        password: _passController.text,
                                        email: _emailController.text,
                                        password_confirmation:
                                            _passController.text)
                                    .toJson());
                              }
                            }, loading: widget.loading),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 15),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translation
                                        .already_have_an_account,
                                    style: TextStyle(
                                      color: AppColors.GRAY_TEXT_COLOR,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            CupertinoPageRoute(
                                                builder: (context) =>
                                                    LoginScreen()));
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translation
                                            .log_in,
                                        style: TextStyle(
                                          color: AppColors.THEME_COLOR,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
