import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/moovle_button.dart';
import 'package:moovle/presenter/widgets/moovle_text_field.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/user_data.dart';

class MakeEnquiryScreen extends StatelessWidget {
  final String id;

  const MakeEnquiryScreen({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
          child: Mutation(
        options: MutationOptions(
            documentNode: MakeEnquiryMutation().document,
            onCompleted: (dynamic resultData) async {
              // TODO Should check if mutation status is success
              if (resultData != null) {
                showDialog<void>(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    return CupertinoAlertDialog(
                      title: Text(
                          AppLocalizations.of(context).translation.success),
                      content: Text(AppLocalizations.of(context)
                          .translation
                          .enquiry_sent),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child:
                              Text(AppLocalizations.of(context).translation.ok),
                          onPressed: () {
                            // TODO Should use popUntil and named routs
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              }
            },
            onError: (exception) {
              showDialog<void>(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: Text(AppLocalizations.of(context)
                        .translation
                        .an_error_has_occurred),
                    content: Text(AppLocalizations.of(context)
                        .translation
                        .something_went_wrong),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child:
                            Text(AppLocalizations.of(context).translation.ok),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            }),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) {
          return MakeEnquiryLayout(
              runMutation: runMutation, loading: result.loading, id: id);
        },
      )),
    ));
  }
}

class MakeEnquiryLayout extends StatefulWidget {
  final bool loading;
  final RunMutation runMutation;
  final String id;

  const MakeEnquiryLayout({Key key, this.loading, this.runMutation, this.id})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MakeEnquiryLayoutState();
  }
}

class _MakeEnquiryLayoutState extends State<MakeEnquiryLayout> {
  TextEditingController _emailController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _phoneNumberController;
  TextEditingController _pleaseSendController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    UserData.instance.then((UserData value) {
      var email = '', firstName = '', lastName = '', phone = '';
      if (value.user != null) {
        email = value.user['email'];
        firstName = value.user['first_name'];
        lastName = value.user['last_name'];
        phone = value.user['phone'];
      }
      setState(() {
        _emailController = TextEditingController(text: email);
        _firstNameController = TextEditingController(text: firstName);
        _lastNameController = TextEditingController(text: lastName);
        _phoneNumberController = TextEditingController(text: phone);
      });
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_pleaseSendController == null) {
      _pleaseSendController = TextEditingController(
          text: AppLocalizations.of(context).translation.please_send_me_more +
              widget.id);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_emailController == null ||
        _firstNameController == null ||
        _lastNameController == null ||
        _phoneNumberController == null) {
      return Center(child: CupertinoActivityIndicator());
    }
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 9),
                    alignment: Alignment.centerLeft,
                    child: CloseButton(),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translation
                                  .make_an_enquiry,
                              style: TextStyle(
                                  color: AppColors.BLACK_COLOR,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 35),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .first_name,
                              AppLocalizations.of(context)
                                  .translation
                                  .first_name,
                              TextInputType.text,
                              controller: _firstNameController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 35),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context).translation.surname,
                              AppLocalizations.of(context).translation.surname,
                              TextInputType.text,
                              controller: _lastNameController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .email_address,
                              AppLocalizations.of(context)
                                  .translation
                                  .email_placeholder,
                              TextInputType.emailAddress,
                              controller: _emailController,
                              validator: (value) {
                                if (!EmailValidator.validate(value)) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_enter_valid_email;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context)
                                  .translation
                                  .phone_number,
                              AppLocalizations.of(context)
                                  .translation
                                  .enter_phone_number,
                              TextInputType.phone,
                              controller: _phoneNumberController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: MoovleTextField(
                              AppLocalizations.of(context).translation.message,
                              '',
                              TextInputType.text,
                              maxLines: 5,
                              controller: _pleaseSendController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .please_fill_out_the_field;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 40),
                            child: MoovleButton(
                              AppLocalizations.of(context)
                                  .translation
                                  .make_enquiry,
                              () {
                                // TODO should hide focus
                                if (_formKey.currentState.validate()) {
                                  widget.runMutation(MakeEnquiryArguments(
                                    first_name: _firstNameController.text,
                                    last_name: _lastNameController.text,
                                    email: _emailController.text,
                                    phone: _phoneNumberController.text,
                                    message: _pleaseSendController.text,
                                    listing_id: int.parse(widget.id),
                                  ).toJson());
                                }
                              },
                              loading: widget.loading,
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
