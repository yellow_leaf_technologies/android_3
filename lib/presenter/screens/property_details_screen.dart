import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:flutter_svg/svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/presenter/screens/make_enquiry_screen.dart';
import 'package:moovle/presenter/widgets/save_button.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/format_currency.dart';

const imagePlaceholder = 'assets/images/img/home-placeholder.png';

class PropertyDetailsScreen extends StatelessWidget {
  final String id;

  const PropertyDetailsScreen({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
            child: Center(
      child: Query(
        options: QueryOptions(
            documentNode: PropertyQuery().document,
            variables: PropertyArguments(id: id).toJson()),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      right: 16,
                      left: 16,
                      top: MediaQuery.of(context).padding.top,
                      bottom: 24),
                  child: CustomBackButton(
                    color: AppColors.DARK_GRAY_COLOR,
                  ),
                ),
                Center(child: ExceptionView(refetch: refetch)),
              ],
            );
          }

          if (result.loading) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      right: 16,
                      left: 16,
                      top: MediaQuery.of(context).padding.top,
                      bottom: 24),
                  child: CustomBackButton(
                    color: AppColors.DARK_GRAY_COLOR,
                  ),
                ),
                Center(child: CupertinoActivityIndicator()),
              ],
            );
          }

          final data = PropertyQuery().parse(result.data);
          return PropertyDetailsLayout(
            data: data.listing,
//          moreProperties: data.near.data,
          );
        },
      ),
    )));
  }
}

class PropertyDetailsLayout extends StatefulWidget {
  final Property$Query$Listing data;
//  final List<Property$Query$ListingPaginator$Listing> moreProperties;

  const PropertyDetailsLayout({Key key, this.data}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PropertyDetailsLayoutState();
  }
}

class _PropertyDetailsLayoutState extends State<PropertyDetailsLayout> {
  @override
  Widget build(BuildContext context) {
    var description = _composeDescription();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
            child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Images(data: widget.data),
              Padding(
                  padding: EdgeInsets.only(left: 24, top: 24),
                  child: Text(
                    widget.data.title,
                    style: TextStyle(fontSize: 26, fontWeight: FontWeight.w700),
                  )),
              Equipment(data: widget.data),
              if (description != null)
                Padding(
                    padding: EdgeInsets.only(left: 24, top: 34, right: 24),
                    child: Text(
                      description,
                      style: TextStyle(
                          fontSize: 14, color: AppColors.GRAY_TEXT_COLOR),
                    )),
              SizedBox(height: 24),
              Amenities(data: widget.data.amenities),
              if (widget.data.latitude != null && widget.data.longitude != null)
                Location(lat: widget.data.latitude, lng: widget.data.longitude),
              //CommunityDistancing(),
              //Neighbourhood(),
//              PricesInArea(
//                price: widget.data.price,
//                averagePrice: widget.data.averagePrice,
//                percentage: widget.data.percentage,
//              ),
//              MoreProperties(
//                moreProperties: widget.moreProperties,
//              )
            ],
          ),
        )),
        Footer(
            price: widget.data.price,
//          available: widget.data.available,
            available: null,
            id: widget.data.id,
            isMessaged: widget.data.isMessaged),
      ],
    );
  }

  String _composeDescription() {
    var en =
        widget.data.descriptions.where((element) => element.language == 'en');
    var description = '';
    if (en.length > 0) {
      description = en.first?.description;
    } else if (widget.data.descriptions.length > 0) {
      description = widget.data.descriptions?.first?.description;
    }
    return description;
  }
}

class Images extends StatefulWidget {
  final Property$Query$Listing data;

  const Images({Key key, this.data}) : super(key: key);

  @override
  _ImagesState createState() => _ImagesState();
}

class _ImagesState extends State<Images> {
  int currentIndex = 0;
  SwiperController _controllerSwiper = SwiperController();

  List<Widget> getIndicatorsWidgets() {
    List<Widget> result = [];
    bool isActive = false;
    for (int i = 0; i < widget.data.photos.length; i++) {
      isActive = currentIndex == i ? true : false;
      result.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.0),
          child: Container(
            height: 8,
            width: 8,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isActive ? AppColors.THEME_COLOR : AppColors.GREY_WHITE,
            ),
          )));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    const maxImageWidth = 328.0;
    const maxImageHeight = 205.0;
    double itemWidth = MediaQuery.of(context).size.width;
    double itemHeight = itemWidth * (maxImageHeight / maxImageWidth);

    Widget photoView;
    if (widget.data.photos == null || widget.data.photos.length == 0) {
      photoView = Image.asset(
        imagePlaceholder,
        fit: BoxFit.cover,
        width: MediaQuery.of(context).size.width,
        height: itemHeight,
      );
    } else if (widget.data.photos.length == 1) {
      photoView = CachedNetworkImage(
        imageUrl: widget.data.photos.first.url,
        fit: BoxFit.cover,
        width: MediaQuery.of(context).size.width,
        height: itemHeight,
        placeholder: (context, url) => Image.asset(
          imagePlaceholder,
          fit: BoxFit.cover,
          width: MediaQuery.of(context).size.width,
          height: itemHeight,
        ),
        errorWidget: (context, url, error) => Image.asset(
          imagePlaceholder,
          fit: BoxFit.cover,
          width: MediaQuery.of(context).size.width,
          height: itemHeight,
        ),
      );
    } else {
      photoView = Swiper(
          physics: BouncingScrollPhysics(),
          itemCount: widget.data.photos.length,
          layout: SwiperLayout.CUSTOM,
          loop: false,
          outer: true,
          controller: _controllerSwiper,
          customLayoutOption: CustomLayoutOption(
            startIndex: -1,
            stateCount: 3,
          ).addTranslate([
            new Offset(-itemWidth, 0.0),
            new Offset(0.0, 0.0),
            new Offset(itemWidth, 0.0)
          ]).addScale([0.8, 1, 0.8], Alignment.center),
          onIndexChanged: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          itemWidth: itemWidth,
          itemHeight: itemHeight,
          itemBuilder: (context, index) {
            String item = widget.data.photos[index].url;
            return Stack(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: item,
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width,
                  height: itemHeight,
                  placeholder: (context, url) => Image.asset(
                    imagePlaceholder,
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                    height: itemHeight,
                  ),
                  errorWidget: (context, url, error) => Image.asset(
                    imagePlaceholder,
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                    height: itemHeight,
                  ),
                )
              ],
            );
          });
    }

    return Stack(
      children: <Widget>[
        photoView,
        Positioned.fill(
          top: MediaQuery.of(context).padding.top,
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                  child: Padding(
                      padding: EdgeInsets.only(bottom: 18),
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: getIndicatorsWidgets(),
                          )))),
              Positioned.fill(
                  child: Padding(
                      padding: EdgeInsets.only(right: 16, left: 16),
                      child: Align(
                          alignment: Alignment.topCenter,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              CustomBackButton(),
                              Spacer(),
                              if (widget.data.latitude != null &&
                                  widget.data.longitude != null)
                                CupertinoButton(
                                    padding: const EdgeInsets.only(top: 4),
                                    onPressed: () {
                                      MapsLauncher.launchCoordinates(
                                          widget.data.latitude,
                                          widget.data.longitude);
                                    },
                                    child: SvgPicture.asset(
                                      'assets/images/svg/ic_pin.svg',
                                      color: Colors.white,
                                      height: 20,
                                    )),
                              SaveButton(
                                  id: widget.data.id,
                                  saved: widget.data.isFavorite),
                            ],
                          )))),
              Positioned.fill(
                  child: Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                              onTap: () {
                                _controllerSwiper.next(animation: true);
                              },
                              child: SvgPicture.asset(
                                'assets/images/svg/ic_arrow_forward.svg',
                                color: Colors.white,
                              ))))),
              Positioned.fill(
                  child: Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                              onTap: () {
                                _controllerSwiper.previous(animation: true);
                              },
                              child: SvgPicture.asset(
                                'assets/images/svg/ic_arrow_backward.svg',
                                color: Colors.white,
                              ))))),
            ],
          ),
        )
      ],
    );
  }
}

class Location extends StatelessWidget {
  final Completer<GoogleMapController> _controller = Completer();
  final CameraPosition _kCurrentPosition;
  final num lat;
  final num lng;

  Location({Key key, num lat, num lng})
      : _kCurrentPosition = CameraPosition(
          target: LatLng(lat, lng),
          zoom: 17,
        ),
        lat = lat,
        lng = lng,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 24, top: 34, bottom: 24),
            child: Text(
              AppLocalizations.of(context).translation.location,
              style: TextStyle(fontSize: 21),
            )),
        SizedBox(
            height: MediaQuery.of(context).size.width,
            child: GoogleMap(
              mapType: MapType.normal,
              mapToolbarEnabled: false,
              myLocationEnabled: true,
              initialCameraPosition: _kCurrentPosition,
              onMapCreated: (GoogleMapController controller) {
                _onMapCreate(controller);
              },
              onTap: (value) => {MapsLauncher.launchCoordinates(lat, lng)},
              //              onCameraMove: _omCameraMove,
              // circles: _circles,
            )),
      ],
    );
  }

  _onMapCreate(GoogleMapController controller) {
    _controller.complete(controller);
  }
}

// TODO this section is on design but we will finish it later
//class CommunityDistancing extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 34),
//            child: Text(
//              AppLocalizations.of(context).translate('commuting_distance'),
//              style: TextStyle(fontSize: 21),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 12, right: 24),
//            child: Text(
//              AppLocalizations.of(context).translate('commuting_distance_disc'),
//              style: TextStyle(fontSize: 14, color: AppColors.GRAY_TEXT_COLOR),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 22),
//            child: Text(
//              AppLocalizations.of(context)
//                  .translate('commuting_distance_button'),
//              style: TextStyle(fontSize: 14, color: AppColors.THEME_COLOR),
//            )),
//      ],
//    );
//  }
//}

// TODO this section is on design but we will finish it later
//class Neighbourhood extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 44),
//            child: Text(
//              AppLocalizations.of(context).translate('neighbourhood'),
//              style: TextStyle(fontSize: 21),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 12, right: 24),
//            child: Text(
//              'Calm and family-friendly, Etterbeek is a popular choice with Brussels’ international community. It’s close to the EU institutions and has good transport links to the city centre, while its parks – including Cinquantenaire – are a big draw',
//              style: TextStyle(fontSize: 14, color: AppColors.GRAY_TEXT_COLOR),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 17),
//            child: Text(
//              AppLocalizations.of(context).translate('neighbourhood_disc'),
//              style: TextStyle(fontSize: 14, color: AppColors.THEME_COLOR),
//            )),
//      ],
//    );
//  }
//}

// TODO this section is on design but we will finish it later
//class PricesInArea extends StatelessWidget {
//  final num price;
//  final num averagePrice;
//  final num percentage;
//
//  const PricesInArea({Key key, this.price, this.averagePrice, this.percentage})
//      : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      crossAxisAlignment: CrossAxisAlignment.start,
//      children: <Widget>[
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 51),
//            child: Text(
//              AppLocalizations.of(context).translate('rental_prices'),
//              style: TextStyle(fontSize: 21),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 12, right: 24),
//            child: Text(
//              AppLocalizations.of(context).translate('rental_prices_head') +
//                  percentage.toString() +
//                  AppLocalizations.of(context).translate('rental_prices_tail'),
//              style: TextStyle(fontSize: 14, color: AppColors.GRAY_TEXT_COLOR),
//            )),
//        SizedBox(
//          height: 24,
//        ),
//        Container(
//          padding: EdgeInsets.only(left: 29, top: 41, bottom: 49, right: 29),
//          color: AppColors.BLUE_WHITE,
//          width: MediaQuery.of(context).size.width,
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              Text(AppLocalizations.of(context).translate('average_apartment') +
//                  formatCurrency(averagePrice)),
//              SizedBox(height: 18),
//              Container(
//                width: MediaQuery.of(context).size.width / 2,
//                height: 9,
//                color: AppColors.GREY_WHITE,
//              ),
//              SizedBox(height: 23),
//              Container(
//                width: MediaQuery.of(context).size.width,
//                height: 9,
//                color: AppColors.THEME_COLOR,
//              ),
//              SizedBox(
//                height: 12,
//              ),
//              Text(AppLocalizations.of(context).translate('price') +
//                  formatCurrency(price)),
//            ],
//          ),
//        ),
////        Padding(
////            padding: EdgeInsets.only(left: 24, top: 23),
////            child: Text(
////              'Detailed rental price comparison…',
////              style: TextStyle(fontFamily: 'Circular', fontSize: 14, color: AppColors.THEME_COLOR),
////            )),
//      ],
//    );
//  }
//}

class Equipment extends StatelessWidget {
  final Property$Query$Listing data;
  const Equipment({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (data.bedrooms == null &&
        data.bathrooms == null &&
        data.floor_area == null &&
        (data.pet_friendly == null || !data.pet_friendly)) {
      return Container();
    }

    const topPadding = 34.0;

    return Container(
        padding: EdgeInsets.only(top: topPadding),
        height: 62.0 + topPadding,
        child: ListView(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            if (data.bedrooms != null)
              EquipmentItem(
                text: data.bedrooms.toString() +
                    ' ' +
                    AppLocalizations.of(context)
                        .translation
                        .getBedLabel(data.bedrooms),
                icon: 'assets/images/svg/beds.svg',
              ),
            if (data.bathrooms != null)
              EquipmentItem(
                text: data.bathrooms.toString() +
                    ' ' +
                    AppLocalizations.of(context)
                        .translation
                        .getBathLabel(data.bathrooms),
                icon: 'assets/images/svg/baths.svg',
              ),
            if (data.floor_area != null)
              EquipmentItem(
                text: data.floor_area.toString() +
                    AppLocalizations.of(context).translation.area,
                icon: 'assets/images/svg/area.svg',
              ),
            if (data.pet_friendly != null && data.pet_friendly)
              EquipmentItem(
                text: AppLocalizations.of(context).translation.pet_friendly,
                icon: 'assets/images/svg/pet.svg',
              )
          ],
        ));
  }
}

class EquipmentItem extends StatelessWidget {
  final String icon;
  final String text;

  const EquipmentItem({Key key, this.icon, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 26, right: 26),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 24,
              child: SvgPicture.asset(
                icon,
                color: AppColors.GRAY_COLOR,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              text,
              style: TextStyle(fontSize: 12, color: AppColors.GRAY_TEXT_COLOR),
            )
          ],
        ));
  }
}

// TODO this section is on design but we will finish it later
//class MoreProperties extends StatelessWidget {
//  final List<Property$Query$ListingPaginator$Listing> moreProperties;
//
//  const MoreProperties({Key key, this.moreProperties}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      crossAxisAlignment: CrossAxisAlignment.start,
//      children: <Widget>[
//        Padding(
//            padding: EdgeInsets.only(left: 24, top: 34),
//            child: Text(
//              AppLocalizations.of(context).translate('more_properties_area'),
//              style: TextStyle(fontSize: 21),
//            )),
//        Padding(
//            padding: EdgeInsets.only(left: 21, top: 12, right: 21),
//            child: Column(
//              children: moreProperties
//                  .map((i) => Padding(
//                        padding: const EdgeInsets.symmetric(vertical: 30),
//                        child: PropertyListItem(
//                            item: Home$Query$ListingPaginator$Listing.fromJson(
//                                i.toJson())),
//                      ))
//                  .toList(),
//            ))
//      ],
//    );
//  }
//}

class Amenities extends StatefulWidget {
  final List<Property$Query$Listing$Amenity> data;
  static const limit = 6;

  const Amenities({Key key, this.data}) : super(key: key);

  @override
  _AmenitiesState createState() => _AmenitiesState();
}

class _AmenitiesState extends State<Amenities> {
  var showAll = false;

  @override
  Widget build(BuildContext context) {
    if (widget.data.length < 1) {
      return Container();
    }

    const itemStyle =
        const TextStyle(color: AppColors.GRAY_TEXT_COLOR, fontSize: 14);
    final views = List<Widget>();
    var i = 0;
    while (i < widget.data.length && (showAll || i < Amenities.limit)) {
      if (i + 1 <= widget.data.length - 1) {
        views.add(Padding(
          padding: const EdgeInsets.only(bottom: 30),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Text(widget.data[i].name ?? '', style: itemStyle)),
              Expanded(
                  child: Text(widget.data[i + 1].name ?? '', style: itemStyle))
            ],
          ),
        ));
        i += 2;
      } else {
        views.add(Padding(
          padding: const EdgeInsets.only(bottom: 30),
          child: Row(
            children: <Widget>[
              Text(widget.data[i].name ?? '', style: itemStyle)
            ],
          ),
        ));
        i++;
      }
    }

    if (widget.data.length > Amenities.limit) {
      views.add(GestureDetector(
        onTap: () {
          setState(() {
            showAll = !showAll;
          });
        },
        child: Text(
          showAll
              ? AppLocalizations.of(context).translation.less_amenities
              : AppLocalizations.of(context).translation.more_amenities,
          style: TextStyle(fontSize: 14, color: AppColors.THEME_COLOR),
        ),
      ));
    }

    return Container(
      padding: EdgeInsets.only(left: 24, bottom: 43, top: 42),
      color: AppColors.BLUE_WHITE,
      width: MediaQuery.of(context).size.width,
      child:
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: views),
    );
  }
}

class Footer extends StatelessWidget {
  final num price;
  final String available;
  final String id;
  final bool isMessaged;

  const Footer({Key key, this.price, this.available, this.id, this.isMessaged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
//    var formattedDate;
//    if (available != null) {
//      formattedDate =
//          formatDate(DateTime.fromMillisecondsSinceEpoch(int.parse(available)));
//    }
    return Container(
      color: AppColors.BLUE_WHITE,
      width: MediaQuery.of(context).size.width,
      height: 90 + MediaQuery.of(context).padding.bottom,
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 28),
              child: Text(
                formatCurrency(price),
                style: TextStyle(
                  color: AppColors.THEME_COLOR,
                  fontSize: 21,
                ),
              ),
            ),
          ),
          // TODO we don't have available date on server
//          Padding(
//              padding: EdgeInsets.only(left: 28, top: 21),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                mainAxisAlignment: MainAxisAlignment.center,
//                mainAxisSize: MainAxisSize.max,
//                children: <Widget>[
//                  Text(
//                    formatCurrency(price),
//                    style: TextStyle(
//                      color: AppColors.THEME_COLOR,
//                      fontSize: 21,
//                    ),
//                  ),
//                  if (formattedDate != null)
//                    Text(
//                        '${AppLocalizations.of(context).translate('available')}: $formattedDate',
//                        style: TextStyle(
//                            color: AppColors.INPUT_GRAY_COLOR, fontSize: 12))
//                ],
//              )),
          if (!isMessaged)
            _buildMakeButton(context)
          else
            _buildInactiveButton(context)
        ],
      ),
    );
  }

  Widget _buildMakeButton(BuildContext context) => Padding(
        padding: EdgeInsets.only(right: 28, top: 21),
        child: Container(
            height: 48,
            width: 153,
            child: Container(
              child: RawMaterialButton(
                fillColor: AppColors.THEME_COLOR,
                onPressed: () {
                  Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => MakeEnquiryScreen(id: id)));
                },
                elevation: 1,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)),
                child: Text(
                  AppLocalizations.of(context).translation.make_enquiry,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            )),
      );

  Widget _buildInactiveButton(BuildContext context) => Padding(
        padding: EdgeInsets.only(right: 28, top: 21),
        child: Container(
            height: 48,
            width: 153,
            child: Container(
              child: Container(
                decoration: BoxDecoration(
                    color: AppColors.BROWNISH_GREY,
                    borderRadius: BorderRadius.circular(4)),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context).translation.enquiry_made,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )),
      );
}

class CustomBackButton extends StatelessWidget {
  final Color color;

  const CustomBackButton({Key key, this.color = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: SvgPicture.asset(
            'assets/images/svg/ic_back.svg',
            color: color,
          )),
    );
  }
}
