import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:moovle/presenter/screens/home_screen.dart';
import 'package:moovle/presenter/screens/messaged_screen.dart';
import 'package:moovle/presenter/screens/my_properties_screen.dart';
import 'package:moovle/presenter/screens/profile_screen.dart';

class MainTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: [
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.search)),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.heart)),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.conversation_bubble)),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.profile_circled)),
        ],
      ),
      tabBuilder: (context, i) {
        return CupertinoTabView(
          builder: (context) {
            if (i == 0) {
              return HomeScreen();
            } else if (i == 1) {
              return MyPropertiesScreen();
            } else if (i == 2) {
              return MessagedScreen();
            } else if (i == 3) {
              return ProfileScreen();
            }
            return null;
          },
        );
      },
    );
  }
}
