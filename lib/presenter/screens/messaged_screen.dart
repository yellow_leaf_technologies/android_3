import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/presenter/widgets/property_list_item.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth.dart';

class MessagedScreen extends StatefulWidget {
  @override
  _MessagedScreenState createState() => _MessagedScreenState();
}

class _MessagedScreenState extends State<MessagedScreen> {
  Auth auth;

  @override
  void initState() {
    Auth.instance.then((value) => setState(() {
          auth = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;
    if (auth == null) {
      content = CupertinoActivityIndicator();
    } else if (auth.isAuthenticated) {
      content = _buildLoader();
    } else {
      content = Text(AppLocalizations.of(context).translation.please_login);
    }

    return CupertinoPageScaffold(
        child: SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 24, bottom: 14, left: 23),
            child: Text(
              AppLocalizations.of(context).translation.messaged,
              style: TextStyle(fontSize: 26, color: AppColors.BLACK_COLOR),
            ),
          ),
          Expanded(
            child: Center(
              child: content,
            ),
          ),
        ],
      ),
    ));
  }

  Widget _buildLoader() {
    return StableLoadBuilder(
      stabilize: false,
      query: MessagedListingsQuery(),
      builder: (Map data, fetchMore, {bool loading}) {
        var result = MessagedListingsQuery().parse(data);
        var list = result.me.messaged_listings;
        if (list.length == 0) {
          return ContainerNoEmpty();
        }

        return ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: PropertyListItem(
                  item: Home$Query$ListingPaginator$Listing.fromJson(
                      list[index].toJson()),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
