import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/presenter/widgets/property_list_item.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth.dart';

class MyPropertiesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 28, bottom: 18, left: 23),
            child: Text(AppLocalizations.of(context).translation.my_properties,
                style: TextStyle(
                    color: AppColors.BLACK_COLOR,
                    fontSize: 26,
                    fontWeight: FontWeight.bold)),
          ),
          Expanded(
              child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(child: PropertiesList()),
            ],
          ))
        ],
      ),
    ));
  }
}

class PropertiesList extends StatefulWidget {
//  final PropertiesArguments arguments;
//
//  const PropertiesList(this.arguments, {Key key}) : super(key: key);
  @override
  _PropertiesListState createState() => _PropertiesListState();
}

class _PropertiesListState extends State<PropertiesList> {
  Auth auth;

  @override
  void initState() {
    Auth.instance.then((value) => setState(() {
          auth = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;
    if (auth == null) {
      content = CupertinoActivityIndicator();
    } else if (auth.isAuthenticated) {
      content = _buildLoader();
    } else {
      content = Text(AppLocalizations.of(context).translation.please_login);
    }

    return Center(
      child: content,
    );
  }

  Widget _buildLoader() {
    return StableLoadBuilder(
      stabilize: false,
      query: FavoritedListingsQuery(),
      builder: (Map data, fetchMore, {bool loading}) {
        var result = FavoritedListingsQuery().parse(data);
        var list = result.me.favorite_listings;
        if (list.length == 0) {
          return ContainerNoEmpty();
        }

        return ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: PropertyListItem(
                  item: Home$Query$ListingPaginator$Listing.fromJson(
                      list[index].toJson()),
                ),
              ),
            );
          },
        );
      },
    );
  }
}

// TODO need to add "matches" tab
class MyPropertiesTabView extends StatefulWidget {
  @override
  _MyPropertiesTabViewState createState() => _MyPropertiesTabViewState();
}

class _MyPropertiesTabViewState extends State<MyPropertiesTabView> {
  var current = 0;

  @override
  Widget build(BuildContext context) {
    var localization = AppLocalizations.of(context);
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            TabButton(
              title: localization.translation.saved,
              active: current == 0,
              onTap: () {
                setState(() {
                  current = 0;
                });
              },
            ),
            TabButton(
              title: localization.translation.matches,
              active: current == 1,
              onTap: () {
                setState(() {
                  current = 1;
                });
              },
            )
          ],
        ),
        Expanded(
            child: PropertiesList(
//          current == 0
//              ? PropertiesArguments(availableNow: true)
//              : PropertiesArguments(petFriendly: true),
                )),
      ],
    );
  }
}

class TabButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  final bool active;

  const TabButton({Key key, this.title, this.onTap, this.active})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle;
    Border border;
    if (active) {
      textStyle = TextStyle(fontSize: 14, color: AppColors.THEME_COLOR);
      border =
          Border(bottom: BorderSide(width: 2, color: AppColors.THEME_COLOR));
    } else {
      textStyle = TextStyle(fontSize: 14, color: AppColors.BLACK_COLOR);
      border = Border(bottom: BorderSide(width: 2, color: Color(0x00ffffff)));
    }

    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(border: border),
        alignment: Alignment.center,
        child: GestureDetector(
          child: Text(
            title,
            style: textStyle,
          ),
          onTap: onTap,
        ),
      ),
    );
  }
}
