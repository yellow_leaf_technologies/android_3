import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/presenter/widgets/infinity_list.dart';
import 'package:moovle/presenter/widgets/property_list_item.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';

class SuggestedPropertyListScreen extends StatefulWidget {
  final String title;
  final Map<String, dynamic> args;

  const SuggestedPropertyListScreen({Key key, this.title, this.args})
      : super(key: key);

  @override
  _SuggestedPropertyListScreenState createState() =>
      _SuggestedPropertyListScreenState();
}

class _SuggestedPropertyListScreenState
    extends State<SuggestedPropertyListScreen> {
  var page = 1;
  Map<String, dynamic> args;
  SearchListingQuery query = SearchListingQuery();

  @override
  void initState() {
    args = SearchListingArguments(page: page, first: 10, search: '').toJson();
    widget.args.removeWhere((key, value) => value == null);
    args.addAll(widget.args);
    args.removeWhere((key, value) => value == null);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
          child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 10, left: 9),
            alignment: Alignment.centerLeft,
            child: CloseButton(),
          ),
          Container(
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 10),
            alignment: Alignment.centerLeft,
            child: Text(
              widget.title,
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: StableLoadBuilder(
                query: query,
                args: args,
                builder: (Map data, fetchMore, {bool loading}) {
                  var result = query.parse(data);
                  var list = result.searchListing.data;
                  if (list.length == 0) {
                    return ContainerNoEmpty();
                  }

                  return InfinityList(
                      fetchMore: () {
                        if (!loading) {
                          page++;
                          args["page"] = page;
                          fetchMore(FetchMoreOptions(
                            documentNode: query.document,
                            variables: args,
                            updateQuery: (
                              dynamic previousResultData,
                              dynamic fetchMoreResultData,
                            ) {
                              var previous = query.parse(previousResultData);
                              var more = query.parse(fetchMoreResultData);
                              previous.searchListing.data
                                  .addAll(more.searchListing.data);
                              previous.searchListing.paginatorInfo =
                                  more.searchListing.paginatorInfo;
                              return previous.toJson();
                            },
                          ));
                        }
                      },
                      length: list.length,
                      hasMorePages:
                          result.searchListing.paginatorInfo.hasMorePages,
                      builder: (index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 30),
                          child: Center(
                              child: PropertyListItem(
                                  item: Home$Query$ListingPaginator$Listing
                                      .fromJson(list[index].toJson()))),
                        );
                      });
                }),
          ),
        ],
      )),
    ));
  }
}
