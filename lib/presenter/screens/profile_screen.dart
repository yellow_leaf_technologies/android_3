import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:moovle/presenter/screens/welcome_screen.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth.dart';
import 'package:moovle/services/compose_name.dart';
import 'package:moovle/services/user_data.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Auth auth;
  UserData userData;

  @override
  void initState() {
    super.initState();
    Auth.instance.then((instance) {
      setState(() {
        auth = instance;
      });
    });
    UserData.instance.then((instance) {
      setState(() {
        userData = instance;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: SafeArea(
            child: Center(
                child: (auth == null || userData == null)
                    ? CupertinoActivityIndicator()
                    : _body)));
  }

  Widget get _body => Container(
        margin: EdgeInsets.only(left: 18, right: 22),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 66,
            ),
            ClipRRect(
                borderRadius: BorderRadius.circular(49.0),
                child: _buildImage()),
            SizedBox(
              height: 13,
            ),
            if (auth.isAuthenticated)
              Text(
                composeName(userData),
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.normal,
                    color: AppColors.BLACK_COLOR),
              ),
            SizedBox(
              height: 52,
            ),
            if (auth.isAuthenticated)
              itemElement('', AppLocalizations.of(context).translation.log_out,
                  () async {
                await auth.reset();
              })
            else
              itemElement('', AppLocalizations.of(context).translation.log_in,
                  () {
                Navigator.of(context, rootNavigator: true)
                    .push(CupertinoPageRoute(builder: (context) {
                  return WelcomeScreen();
                }));
              }),
            Container(
              height: 1,
              color: AppColors.GREY_LINE,
            )
          ],
        ),
      );

  Widget itemElement(String icon, String title, VoidCallback onPressed) {
    return GestureDetector(
        onTap: onPressed,
        child: Column(
          children: <Widget>[
            Container(
              height: 1,
              color: AppColors.GREY_LINE,
            ),
            SizedBox(
              height: 17,
            ),
            Row(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      color: AppColors.BLACK_COLOR),
                )
              ],
            ),
            SizedBox(
              height: 14,
            ),
          ],
        ));
  }

  Widget _buildImage() {
    if (auth.isAuthenticated &&
        userData.user['avatar'] != null &&
        userData.user['avatar'] != '') {
      return CachedNetworkImage(
        imageUrl: userData.user['avatar'],
        width: 98,
        height: 98,
        fit: BoxFit.cover,
        placeholder: (context, url) => Container(
            width: 98,
            height: 98,
            child: Center(
              child: CircularProgressIndicator(),
            )),
        errorWidget: (context, url, error) =>
            Image.asset('assets/images/png/ic_male.png'),
      );
    } else {
      return Image.asset(
        'assets/images/png/ic_male.png',
        width: 98,
        height: 98,
      );
    }
  }
}
