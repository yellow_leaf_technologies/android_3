import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moovle/presenter/screens/login_screen.dart';
import 'package:moovle/presenter/screens/signup_screen.dart';
import 'package:moovle/presenter/widgets/gesture_text_view.dart';
import 'package:moovle/presenter/widgets/moovle_button.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth_social_services.dart';
import 'package:moovle/services/home_image.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: WelcomeLayout(),
    );
  }
}

class WelcomeLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final maxImageHeight = HomeImage.maxImageHeight(context);
    final maxImageWidth = HomeImage.maxImageWidth(context);
    // Height what should fit main screen view
    const minMainViewHeight = 326.0;

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        final scale = maxImageWidth / viewportConstraints.maxWidth;
        final imageHeight = maxImageHeight / scale;

        final availableSpace =
            viewportConstraints.maxHeight - minMainViewHeight;
        var containerHeight;
        if (availableSpace < imageHeight) {
          containerHeight = availableSpace;
        } else {
          containerHeight = imageHeight;
        }
        var mainViewHeight = viewportConstraints.maxHeight - containerHeight;

        return Column(
          children: <Widget>[
            Container(
              height: containerHeight,
              alignment: Alignment.bottomCenter,
              child: OverflowBox(
                alignment: Alignment.bottomCenter,
                minWidth: viewportConstraints.maxWidth,
                maxWidth: viewportConstraints.maxWidth,
                minHeight: imageHeight,
                maxHeight: imageHeight,
                child: Image.asset(
                  'assets/images/img/homepage.jpg',
                ),
              ),
            ),
            Container(
              height: mainViewHeight,
              child: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 35, vertical: 35),
                    child: SafeArea(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 40),
                            child: SvgPicture.asset(
                                'assets/images/svg/logo.svg',
                                semanticsLabel: 'Moovle Logo'),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 40),
                            child: MoovleButton(
                                AppLocalizations.of(context)
                                    .translation
                                    .lets_find_home, () {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => LoginScreen()));
                            }),
                          ),
                          Container(
                            child: Text(
                              AppLocalizations.of(context)
                                  .translation
                                  .to_get_create_an_account_desc,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.GRAY_TEXT_COLOR,
                                fontSize: 14,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 40),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: Center(
                                    child: GestureTextView(
                                        AppLocalizations.of(context)
                                            .translation
                                            .sign_up, () {
                                      Navigator.push(
                                          context,
                                          CupertinoPageRoute(
                                              builder: (context) =>
                                                  SignUpScreen()));
                                    }),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                      child: GestureTextView(
                                          AppLocalizations.of(context)
                                              .translation
                                              .log_in, () {
                                    Navigator.push(
                                        context,
                                        CupertinoPageRoute(
                                            builder: (context) =>
                                                LoginScreen()));
                                  })),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 40),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: Center(
                                    child:
                                        GestureTextView('Facebook', () async {
                                      AuthSocialServices(
                                              AuthSocialStatus.facebook)
                                          .loginUser()
                                          .then((result) => {});
                                    }),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                      child: GestureTextView('Google', () {
                                    AuthSocialServices(AuthSocialStatus.google)
                                        .loginUser()
                                        .then((result) => {});
                                  })),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
