import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/custom_check_box.dart';
import 'package:moovle/presenter/widgets/range_slider_data.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class FilterSearchScreen extends StatelessWidget {
  final Map<dynamic, dynamic> filters;

  const FilterSearchScreen({Key key, this.filters}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
          child: SafeArea(child: FilterSearchLayout(filters: filters))),
    );
  }
}

class FilterSearchLayout extends StatefulWidget {
  final Map<dynamic, dynamic> filters;

  const FilterSearchLayout({Key key, this.filters}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FilterSearchLayoutState();
  }
}

class _FilterSearchLayoutState extends State<FilterSearchLayout> {
  // TODO need to refactor, to not to rely on strings
  static const bedroomsValues = {'any', '1', '2', '3', '4_plus'};
  static const bathroomsValues = {'any', '1', '2', '3+'};
  static const petsValues = {'any', 'yes', 'no'};
  static const propertyValues = {'any', 'apartment', 'house'};
  static const parkingValues = {'any', 'garage', 'off_street', 'street'};
  static const furnishedValues = {'any', 'furnished'};

  int minPrice = 0;
  int maxPrice = 3600;
  int lowerValuePriceResult = 0;
  int upperValuePriceResult = 3600;
  String propertySelectKey = 'any';
  String bedrooomsSelectKey = 'any';
  String bathroomsSelectKey = 'any';
  String parkingSelectKey = 'any';
  String furnitureSelectKey = 'any';
  String petsSelectKey = 'any';

  Map<String, dynamic> amenitiesMapResult = Map();

  @override
  void initState() {
    try {
      _adaptFromApi();
    } catch (e) {
      // we have default values on define
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10, left: 9),
                  alignment: Alignment.centerLeft,
                  child: CloseButton(resultComposer: () {
                    try {
                      return _adaptToApi();
                    } catch (e) {
                      return Map<String, dynamic>();
                    }
                  }),
                ),
                Padding(
                    padding: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                            AppLocalizations.of(context)
                                .translation
                                .filter_your_search,
                            style: TextStyle(
                                color: AppColors.BLACK_COLOR,
                                fontWeight: FontWeight.bold,
                                fontSize: 25),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          AppLocalizations.of(context).translation.price_range,
                          style: TextStyle(color: AppColors.BROWNISH_GREY),
                        ),
                        SizedBox(
                          height: 21,
                        ),
                        Text(
                          '$lowerValuePriceResult - ${upperValuePriceResult == maxPrice ? 'Any' : upperValuePriceResult}',
                          style: TextStyle(
                              color: AppColors.BROWNISH_GREY,
                              fontSize: 16,
                              fontFamily: 'Circular'),
                        ),
                        SizedBox(
                          height: 60,
                          child: RangeSliderData(
                              min: minPrice.toDouble(),
                              max: maxPrice.toDouble(),
                              showValueIndicator: false,
                              lowerValue: lowerValuePriceResult.toDouble(),
                              upperValue: upperValuePriceResult.toDouble(),
                              callback:
                                  (double newLowerValue, double newUpperValue) {
                                setState(() {
                                  lowerValuePriceResult = newLowerValue.floor();
                                  upperValuePriceResult = newUpperValue.floor();
                                });
                              }),
                        ),
                        SizedBox(
                          height: 68,
                        ),
                        _propertyType,
                        SizedBox(
                          height: 64,
                        ),
                        _bedrooomsType,
                        SizedBox(
                          height: 64,
                        ),
                        _bathroomsType,
                        SizedBox(
                          height: 64,
                        ),
                        _parkingType,
                        SizedBox(
                          height: 64,
                        ),
                        _furnitureType,
                        SizedBox(
                          height: 64,
                        ),
                        _petsType,
                        SizedBox(
                          height: 64,
                        ),
                        CardFeaturesItem(
                          amenitiesMapResult: amenitiesMapResult,
                        ),
                        SizedBox(
                          height: 24,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget get _propertyType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.property_type,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
              children: propertyValues
                  .map((entry) => itemContainer(propertySelectKey, entry, () {
                        setState(() {
                          propertySelectKey = entry;
                        });
                      }, propertyValues.length))
                  .toList())
        ],
      );

  Widget get _bedrooomsType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.bedroooms,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
            children: bedroomsValues
                .map((entry) => itemContainer(bedrooomsSelectKey, entry, () {
                      setState(() {
                        bedrooomsSelectKey = entry;
                      });
                    }, bedroomsValues.length))
                .toList(),
          )
        ],
      );

  Widget get _bathroomsType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.bathrooms,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
              children: bathroomsValues
                  .map((entry) => itemContainer(bathroomsSelectKey, entry, () {
                        setState(() {
                          bathroomsSelectKey = entry;
                        });
                      }, bathroomsValues.length))
                  .toList())
        ],
      );

  Widget get _parkingType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.parking,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
              children: parkingValues
                  .map((entry) => itemContainer(parkingSelectKey, entry, () {
                        setState(() {
                          parkingSelectKey = entry;
                        });
                      }, parkingValues.length))
                  .toList())
        ],
      );

  Widget get _furnitureType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.furniture,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
              children: furnishedValues
                  .map((entry) => itemContainer(furnitureSelectKey, entry, () {
                        setState(() {
                          furnitureSelectKey = entry;
                        });
                      }, furnishedValues.length))
                  .toList())
        ],
      );

  setStateFurniture(String key) {
    setState(() {
      furnitureSelectKey = key;
    });
  }

  Widget get _petsType => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translation.pets,
            style: TextStyle(
                color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
              children: petsValues
                  .map((entry) => itemContainer(petsSelectKey, entry, () {
                        setState(() {
                          petsSelectKey = entry;
                        });
                      }, petsValues.length))
                  .toList())
        ],
      );

  Widget itemContainer(
      String selectKey, String key, VoidCallback onPressed, int count) {
    return InkWell(
        onTap: onPressed,
        child: Container(
          decoration: BoxDecoration(
            color:
                selectKey == key ? AppColors.THEME_COLOR : Colors.transparent,
            border: Border.all(
              color: Color(0xFFe6e6e6),
              width: selectKey == key ? 0 : 1,
            ),
          ),
          width: (MediaQuery.of(context).size.width - 43) / count,
          padding: EdgeInsets.only(top: 13, bottom: 14),
          child: Text(
            _keyToTranslation(key, AppLocalizations.of(context)),
            textAlign: TextAlign.center,
            style: TextStyle(
                color:
                    selectKey == key ? Colors.white : AppColors.GRAY_TEXT_COLOR,
                fontFamily: 'Circular'),
          ),
        ));
  }

  String _keyToTranslation(String key, AppLocalizations localization) {
    switch (key) {
      case 'any':
        return localization.translation.any;
      case '1':
        return localization.translation.one;
      case '2':
        return localization.translation.two;
      case '3':
        return localization.translation.three;
      case '4_plus':
        return localization.translation.four_plus;
      case '3+':
        return localization.translation.three_plus;
      case 'yes':
        return localization.translation.yes;
      case 'no':
        return localization.translation.no;
      case 'apartment':
        return localization.translation.apartment;
      case 'house':
        return localization.translation.house;
      case 'garage':
        return localization.translation.garage;
      case 'off_street':
        return localization.translation.off_street;
      case 'street':
        return localization.translation.street;
      case 'furnished':
        return localization.translation.furnished;
      default:
        return '';
    }
  }

  void _adaptFromApi() {
    if (widget.filters['bedrooms_more_than'] == 3) {
      bedrooomsSelectKey = '4_plus';
    } else if (widget.filters['bedrooms'] != null) {
      bedrooomsSelectKey = widget.filters['bedrooms'].toString();
    } else {
      bedrooomsSelectKey = 'any';
    }

    if (widget.filters['bathrooms_more_than'] == 2) {
      bathroomsSelectKey = '3+';
    } else if (widget.filters['bathrooms'] != null) {
      bathroomsSelectKey = widget.filters['bathrooms'].toString();
    } else {
      bathroomsSelectKey = 'any';
    }

    if (widget.filters['price_from'] != null) {
      lowerValuePriceResult = widget.filters['price_from'];
    } else {
      lowerValuePriceResult = 0;
    }
    if (widget.filters['price_to'] != null) {
      upperValuePriceResult = widget.filters['price_to'];
    } else {
      upperValuePriceResult = maxPrice;
    }

    if (widget.filters['pet_friendly'] == true) {
      petsSelectKey = 'yes';
    } else if (widget.filters['pet_friendly'] == false) {
      petsSelectKey = 'no';
    }

    if (widget.filters['listing_type_id'] == 1) {
      propertySelectKey = 'apartment';
    } else if (widget.filters['listing_type_id'] == 2) {
      propertySelectKey = 'house';
    }

    if (widget.filters['parking_type_id'] == 1) {
      parkingSelectKey = 'garage';
    } else if (widget.filters['parking_type_id'] == 2) {
      parkingSelectKey = 'off_street';
    } else if (widget.filters['parking_type_id'] == 3) {
      parkingSelectKey = 'street';
    }

    if (widget.filters['amenities'] != null) {
      var amenities = [];
      if (widget.filters['amenities'] is List) {
        amenities = widget.filters['amenities'] as List;
      }

      if (amenities.contains(38)) {
        furnitureSelectKey = 'furnished';
      }
      amenities.forEach((element) {
        if (element != '38' && element != 38) {
          amenitiesMapResult[element.toString()] = true;
        }
      });
    }
  }

  Map<String, dynamic> _adaptToApi() {
    var result = <String, dynamic>{};

    if (bedrooomsSelectKey == '4_plus') {
      result['bedrooms_more_than'] = 3;
    } else if (bedrooomsSelectKey != 'any') {
      result['bedrooms'] = int.parse(bedrooomsSelectKey);
    }

    if (bathroomsSelectKey == '3+') {
      result['bathrooms_more_than'] = 2;
    } else if (bathroomsSelectKey != 'any') {
      result['bathrooms'] = int.parse(bathroomsSelectKey);
    }
    result['price_from'] = lowerValuePriceResult;
    if (upperValuePriceResult < maxPrice) {
      result['price_to'] = upperValuePriceResult;
    }

    if (petsSelectKey == 'yes') {
      result['pet_friendly'] = true;
    } else if (petsSelectKey == 'no') {
      result['pet_friendly'] = false;
    }

    if (propertySelectKey == 'apartment') {
      result['listing_type_id'] = 1;
    } else if (propertySelectKey == 'house') {
      result['listing_type_id'] = 2;
    }

    if (parkingSelectKey == 'garage') {
      result['parking_type_id'] = 1;
    } else if (parkingSelectKey == 'off_street') {
      result['parking_type_id'] = 2;
    } else if (parkingSelectKey == 'street') {
      result['parking_type_id'] = 3;
    }

    var amenities = [];
    if (furnitureSelectKey == 'furnished') {
      amenities.add(38);
    }

    final selectedAmenities = amenitiesMapResult.entries
        .where((element) => element.value)
        .map((element) => element.key);
    amenities.addAll(selectedAmenities);

    if (amenities.isNotEmpty) {
      result['amenities'] = amenities;
    }
    return result;
  }
}

class CardFeaturesItem extends StatelessWidget {
  final Map<String, dynamic> amenitiesMapResult;

  const CardFeaturesItem({Key key, this.amenitiesMapResult}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StableLoadBuilder(
      query: AmenitiesQuery(),
      builder: (Map<dynamic, dynamic> data,
          dynamic Function(FetchMoreOptions) fetchMore,
          {bool loading}) {
        final result = AmenitiesQuery().parse(data);
        var list = result.amenities.data
            .where((element) => element.id != '38')
            .toList();
        list.forEach((element) {
          if (!amenitiesMapResult.containsKey(element.id)) {
            amenitiesMapResult[element.id] = false;
          }
        });
        return Features(
          amenities: list,
          amenitiesMapResult: amenitiesMapResult,
        );
      },
    );
  }
}

class Features extends StatelessWidget {
  final List<Amenities$Query$AmenityPaginator$Amenity> amenities;
  final Map<String, dynamic> amenitiesMapResult;

  const Features({Key key, this.amenities, this.amenitiesMapResult})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).translation.features,
          style:
              TextStyle(color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'),
        ),
        SizedBox(
          height: 13,
        ),
        GridView.builder(
          shrinkWrap: true,
          itemCount: amenities.length,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height / 9),
            crossAxisSpacing: 3,
            mainAxisSpacing: 3,
          ),
          itemBuilder: (BuildContext context, int index) {
            Amenities$Query$AmenityPaginator$Amenity item = amenities[index];
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CustomCheckBox(
                    value: amenitiesMapResult[item.id],
                    onChanged: (value) {
                      amenitiesMapResult[item.id] =
                          !amenitiesMapResult[item.id];
                      (context as Element).markNeedsBuild();
                    }),
                SizedBox(
                  width: 11,
                ),
                Text(item.name,
                    style: TextStyle(
                        color: AppColors.BROWNISH_GREY, fontFamily: 'Circular'))
              ],
            );
          },
        )
      ],
    );
  }
}
