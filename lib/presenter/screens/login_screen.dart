import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide CloseButton;
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/presenter/screens/forgot_password_screen.dart';
import 'package:moovle/presenter/widgets/close_button.dart';
import 'package:moovle/presenter/widgets/moovle_button.dart';
import 'package:moovle/presenter/widgets/moovle_text_field.dart';
import 'package:moovle/presenter/widgets/password_text_field.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/auth.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
          child: SafeArea(
              child: Mutation(
        options: MutationOptions(
            documentNode: LoginMutation().document,
            onCompleted: (dynamic resultData) async {
              if (resultData != null) {
                var auth = await Auth.instance;
                var data = LoginMutation().parse(resultData).login;
                await auth.setData(token: data.access_token, user: data.user);
                Navigator.of(context, rootNavigator: true).popUntil((route) {
                  return route.settings.name == "/";
                });
              }
            },
            onError: (exception) {
              showDialog<void>(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: Text(AppLocalizations.of(context)
                        .translation
                        .an_error_has_occurred),
                    content: Text(AppLocalizations.of(context)
                        .translation
                        .you_did_not_sign),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child:
                            Text(AppLocalizations.of(context).translation.ok),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            }),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) {
          return LoginLayout(runMutation: runMutation, loading: result.loading);
        },
      ))),
    );
  }
}

class LoginLayout extends StatefulWidget {
  final RunMutation runMutation;
  final bool loading;

  const LoginLayout({Key key, this.runMutation, this.loading})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _LoginLayoutState();
  }
}

class _LoginLayoutState extends State<LoginLayout> {
  TextEditingController _emailController;
  TextEditingController _passController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController(text: '');
    _passController = TextEditingController(text: '');
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 9),
                    alignment: Alignment.centerLeft,
                    child: CloseButton(),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                              AppLocalizations.of(context).translation.log_in,
                              style: TextStyle(
                                  color: AppColors.BLACK_COLOR,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 35),
                              alignment: Alignment.centerLeft,
                              child: MoovleTextField(
                                AppLocalizations.of(context)
                                    .translation
                                    .email_address,
                                AppLocalizations.of(context)
                                    .translation
                                    .email_placeholder,
                                TextInputType.emailAddress,
                                controller: _emailController,
                                validator: (value) {
                                  if (!EmailValidator.validate(value)) {
                                    return AppLocalizations.of(context)
                                        .translation
                                        .please_enter_valid_email;
                                  }
                                  return null;
                                },
                              )),
                          Container(
                            padding: EdgeInsets.only(top: 30),
                            alignment: Alignment.centerLeft,
                            child: PasswordTextField(
                              '* * * * * *',
                              controller: _passController,
                              validator: (value) {
                                if (value.isEmpty || value.length < 8) {
                                  return AppLocalizations.of(context)
                                      .translation
                                      .password_least_character;
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 26, bottom: 16),
                            child: MoovleButton(
                                AppLocalizations.of(context)
                                    .translation
                                    .log_in_find_home, () {
                              if (_formKey.currentState.validate()) {
                                widget.runMutation({
                                  "username": _emailController.text,
                                  "password": _passController.text
                                });
                              }
                            }, loading: widget.loading),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 15),
                            alignment: Alignment.centerLeft,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) =>
                                            ForgotPasswordScreen()));
                              },
                              child: Text(
                                AppLocalizations.of(context)
                                    .translation
                                    .forgot_password,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: AppColors.THEME_COLOR,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
