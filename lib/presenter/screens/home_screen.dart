import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/presenter/screens/search_screen.dart';
import 'package:moovle/presenter/screens/suggested_property_list_screen.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/presenter/widgets/property_list_item.dart';
import 'package:moovle/presenter/widgets/search_field.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/home_image.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final maxImageHeight = HomeImage.maxImageHeight(context);
    final maxImageWidth = HomeImage.maxImageWidth(context);

    return CupertinoPageScaffold(
        child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          LayoutBuilder(builder:
              (BuildContext context, BoxConstraints viewportConstraints) {
            final scale = maxImageWidth / viewportConstraints.maxWidth;
            final imageHeight = maxImageHeight / scale;
            return Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/img/homepage.jpg'))),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 44),
              height: imageHeight,
              width: viewportConstraints.maxWidth,
              alignment: Alignment.bottomCenter,
              child: SearchField(onTap: () {
                Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (BuildContext context, Animation animation,
                      Animation secondaryAnimation) {
                    return SearchScreen();
                  },
                  transitionsBuilder: (
                    BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child,
                  ) {
                    return FadeTransition(
                      opacity: animation,
                      child: child,
                    );
                  },
                  transitionDuration: Duration(milliseconds: 300),
                ));
              }),
            );
          }),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: const HomeSuggestions(),
            ),
          )
        ],
      ),
    ));
  }
}

class HomeSuggestions extends StatelessWidget {
  const HomeSuggestions({key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StableLoadBuilder(
      query: HomeQuery(),
      builder: (Map data, fetchMore, {bool loading}) {
        var response = HomeQuery().parse(data);
        return Column(
          children: <Widget>[
            if (response.availableNow.data.length > 0)
              Suggestion(
                  title: AppLocalizations.of(context).translation.available_now,
                  propertyList: response.availableNow.data,
                  propertiesQueryArguments:
                      SearchListingArguments(first: 10, available: true)
                          .toJson()),
            Presets(),
            if (response.petFriendly.data.length > 0)
              Suggestion(
                  title: AppLocalizations.of(context).translation.pet_friendly,
                  propertyList: response.petFriendly.data,
                  propertiesQueryArguments:
                      SearchListingArguments(first: 10, pet_friendly: 1)
                          .toJson()),
//            if (response.studios.data.length < 0)
//              Suggestion(
//                  title: AppLocalizations.of(context).translate('studios'),
//                  propertyList: response.studios.data,
//                  propertiesQueryArguments: {"studio": true})
          ],
        );
      },
    );
  }
}

class Suggestion extends StatelessWidget {
  final String title;
  final List<Home$Query$ListingPaginator$Listing> propertyList;
  final Map<String, dynamic> propertiesQueryArguments;

  const Suggestion(
      {Key key,
      @required this.title,
      @required this.propertyList,
      this.propertiesQueryArguments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Header(
              title: title, propertiesQueryArguments: propertiesQueryArguments),
          Scroller(propertyList: propertyList)
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  final String title;
  final Map<String, dynamic> propertiesQueryArguments;

  const Header({Key key, @required this.title, this.propertiesQueryArguments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 21,
                color: AppColors.BLACK_COLOR),
          ),
          CupertinoButton(
            child: Text(
              AppLocalizations.of(context).translation.view_all,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: AppColors.THEME_COLOR),
            ),
            onPressed: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) {
                return SuggestedPropertyListScreen(
                    title: title, args: propertiesQueryArguments);
              }));
            },
          ),
        ],
      ),
    );
  }
}

class Scroller extends StatelessWidget {
  final List<Home$Query$ListingPaginator$Listing> propertyList;

  const Scroller({Key key, @required this.propertyList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // height calculated, may differ from design
    return propertyList.length > 0
        ? Container(
            height: 340,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 19),
              scrollDirection: Axis.horizontal,
              children: propertyList
                  .map((item) => PropertyListItem(item: item))
                  .toList(),
            ),
          )
        : Container(height: 340, child: ContainerNoEmpty());
  }
}

class Presets extends StatelessWidget {
  static const maxImageWidth = 328.0;
  static const maxImageHeight = 240.0;

  @override
  Widget build(BuildContext context) {
    double width;
    double height;
    final screenWidth = MediaQuery.of(context).size.width;
    if (screenWidth > maxImageWidth) {
      width = maxImageWidth;
      height = maxImageHeight;
    } else {
      width = screenWidth - widthShrinkCoefficient;
      height = ((width / maxImageWidth) * maxImageHeight).floorToDouble();
    }

    final localization = AppLocalizations.of(context);

    return Container(
      padding: EdgeInsets.only(bottom: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 24, bottom: 16),
            child: Text(
              localization.translation.find_what_you_looking,
              style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: height,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 19),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                PresetItem(
                  title: localization.translation.budget,
                  imageName: localization.translation.budget,
                  args: SearchListingArguments(
                          first: 10, orderBy: SearchListing$SortOrder.ASC)
                      .toJson(),
                  width: width,
                  height: height,
                ),
                PresetItem(
                  title: localization.translation.luxurious,
                  imageName: localization.translation.luxurious,
                  args: SearchListingArguments(
                          first: 10, orderBy: SearchListing$SortOrder.DESC)
                      .toJson(),
                  width: width,
                  height: height,
                ),
                PresetItem(
                  title: localization.translation.furnished,
                  imageName: localization.translation.furnished,
                  args: SearchListingArguments(first: 10, furnished: true)
                      .toJson(),
                  width: width,
                  height: height,
                ),
                PresetItem(
                  title: localization.translation.couples,
                  imageName: localization.translation.couples,
                  args: SearchListingArguments(first: 10, couples: 2).toJson(),
                  width: width,
                  height: height,
                ),
                // TODO we don't have offices on server so I don't know the id
//                PresetItem(
//                  title: localization.translate('offices'),
//                  imageName: localization.translate('offices'),
//                  args: SearchListingArguments(first: 10, listing_type_id: )
//                      .toJson(),
//                  width: width,
//                  height: height,
//                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PresetItem extends StatelessWidget {
  final String title;
  final String imageName;
  final Map<String, dynamic> args;
  final double width;
  final double height;

  const PresetItem(
      {Key key, this.title, this.imageName, this.args, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(CupertinoPageRoute(builder: (context) {
            return SuggestedPropertyListScreen(
              title: title,
              args: args,
            );
          }));
        },
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                'assets/images/img/$imageName.jpg',
                fit: BoxFit.fitHeight,
                width: width,
                height: height,
              ),
            ),
            Positioned.fill(
              child: Center(
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                      decoration: BoxDecoration(
                          color: AppColors.WHITE_COLOR,
                          borderRadius: BorderRadius.circular(4)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            title,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: AppColors.THEME_COLOR),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 14),
                            child: SvgPicture.asset(
                              'assets/images/svg/right_arrow.svg',
                              color: AppColors.THEME_COLOR,
                            ),
                          )
                        ],
                      ))),
            ),
          ],
        ),
      ),
    );
  }
}
