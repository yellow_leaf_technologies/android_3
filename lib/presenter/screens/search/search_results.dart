import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/presenter/widgets/infinity_list.dart';
import 'package:moovle/presenter/widgets/property_list_item.dart';
import 'package:moovle/presenter/widgets/stable_load_builder.dart';

class SearchResults extends StatefulWidget {
  final String phrase;
  final Map<dynamic, dynamic> filters;

  const SearchResults({Key key, this.phrase, this.filters}) : super(key: key);

  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  SearchListingQuery query;
  Map args;
  int page = 1;

  @override
  void initState() {
    query = SearchListingQuery();
    updateArgs();
    super.initState();
  }

  @override
  void didUpdateWidget(SearchResults oldWidget) {
    super.didUpdateWidget(oldWidget);
    updateArgs();
  }

  void updateArgs() {
    try {
      _adaptToApi();
    } catch (e) {
      args = SearchListingArguments(first: 10, search: '').toJson();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: StableLoadBuilder(
        query: query,
        args: args,
        builder: (Map data, fetchMore, {bool loading}) {
          var searchResult = query.parse(data);
          if (searchResult.searchListing.data.length == 0) {
            return ContainerNoEmpty();
          }

          return InfinityList(
              fetchMore: () {
                if (!loading) {
                  page++;
                  args["page"] = page;
                  fetchMore(FetchMoreOptions(
                    documentNode: query.document,
                    variables: args,
                    updateQuery: (
                      dynamic previousResultData,
                      dynamic fetchMoreResultData,
                    ) {
                      var previous = query.parse(previousResultData);
                      var more = query.parse(fetchMoreResultData);
                      previous.searchListing.data
                          .addAll(more.searchListing.data);
                      previous.searchListing.paginatorInfo =
                          more.searchListing.paginatorInfo;
                      return previous.toJson();
                    },
                  ));
                }
              },
              length: searchResult.searchListing.data.length,
              hasMorePages:
                  searchResult.searchListing.paginatorInfo.hasMorePages,
              builder: (index) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 25),
                    child: PropertyListItem(
                      item: Home$Query$ListingPaginator$Listing.fromJson(
                          searchResult.searchListing.data[index].toJson()),
                    ),
                  ),
                );
              });
        },
      ),
    );
  }

  void _adaptToApi() {
    List<int> amenities;
    if (widget.filters['amenities'] != null) {
      amenities = (widget.filters['amenities'] as List)
          .map((e) => e is String ? int.parse(e) : e as int)
          .toList();
    }
    args = SearchListingArguments(
      search: widget.phrase,
      first: 10,
      bathrooms: widget.filters['bathrooms'],
      bathrooms_more_than: widget.filters['bathrooms_more_than'],
      bedrooms: widget.filters['bedrooms'],
      bedrooms_more_than: widget.filters['bedrooms_more_than'],
      listing_type_id: widget.filters['listing_type_id'],
      parking_type_id: widget.filters['parking_type_id'],
//      pet_friendly: widget.filters['pet_friendly'],
      price_from: widget.filters['price_from'],
      price_to: widget.filters['price_to'],
      amenities: amenities,
      page: page,
    ).toJson();
    args.removeWhere((key, value) => value == null);
  }
}
