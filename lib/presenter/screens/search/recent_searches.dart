import 'package:flutter/cupertino.dart';
import 'package:moovle/presenter/screens/search_screen.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/user_data.dart';

class RecentSearches extends StatelessWidget {
  final ShowResults showResults;
  final UserData userData;

  const RecentSearches({Key key, @required this.showResults, this.userData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 16),
          child: Text(
            AppLocalizations.of(context).translation.recent_searches,
            style: TextStyle(fontSize: 12, color: AppColors.GRAY_COLOR),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            child: userData == null
                ? ContainerNoEmpty()
                : ListView(
                    children: userData.history
                        .map((String entry) {
                          return GestureDetector(
                            onTap: () {
                              showResults(value: entry, addToHistory: false);
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                entry,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: AppColors.GRAY_TEXT_COLOR),
                              ),
                            ),
                          );
                        })
                        .toList()
                        .reversed
                        .toList(),
                  ),
          ),
        )
      ],
    );
  }
}
