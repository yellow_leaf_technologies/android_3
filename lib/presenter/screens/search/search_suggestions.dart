import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/screens/search_screen.dart';
import 'package:moovle/presenter/widgets/container_no_empty.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class SearchSuggestions extends StatefulWidget {
  final ShowResults showResults;
  final String phrase;

  const SearchSuggestions({Key key, @required this.showResults, this.phrase})
      : super(key: key);

  @override
  _SearchSuggestionsState createState() => _SearchSuggestionsState();
}

class _SearchSuggestionsState extends State<SearchSuggestions> {
  SearchSuggestion$Query suggestions;
  List<String> items;

  @override
  void didUpdateWidget(SearchSuggestions oldWidget) {
    items = null;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
          documentNode: SearchSuggestionQuery().document,
          variables: SearchSuggestionArguments(phrase: widget.phrase).toJson()),
      builder: (
        QueryResult result, {
        Refetch refetch,
        FetchMore fetchMore,
      }) {
        if (result.data != null && items == null) {
          suggestions = SearchSuggestionQuery().parse(result.data);
          final titles = List<String>();
          final addresses = List<String>();
          final last = List<String>();
          items = List<String>();
          for (var item in suggestions.searchListing.data) {
            final phrase = widget.phrase.toLowerCase();
            if (item.title.toLowerCase().contains(phrase)) {
              if (!titles.contains(item.title)) {
                titles.add(item.title);
              }
            } else if (item.address.toLowerCase().contains(phrase)) {
              if (!addresses.contains(item.address)) {
                addresses.add(item.address);
              }
            } else {
              if (!titles.contains(item.title)) {
                titles.add(item.title);
              }
              if (!addresses.contains(item.address)) {
                addresses.add(item.address);
              }
            }
          }
          items.addAll(titles);
          items.addAll(addresses);
          items.addAll(last);
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 26, vertical: 16),
                  child: Text(
                    AppLocalizations.of(context).translation.search_results,
                    style: TextStyle(fontSize: 12, color: AppColors.GRAY_COLOR),
                  ),
                ),
                if (result.loading) CupertinoActivityIndicator()
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 26),
                child: items == null
                    ? ContainerNoEmpty()
                    : ListView(
                        children: items.map((result) {
                        return GestureDetector(
                          onTap: () {
                            widget.showResults(value: result);
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                // TODO need to update logic
                                Text(
                                  result,
                                  style: TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                            // TODO not clear how to highlight result from server
//                      child: RichText(
//                        text: TextSpan(
//                          text: 'Hello ',
//                          style: DefaultTextStyle.of(context).style,
//                          children: <TextSpan>[
//                            TextSpan(text: 'bold', style: TextStyle(fontWeight: FontWeight.bold)),
//                            TextSpan(text: ' world!'),
//                          ],
//                        ),
//                      ),
                          ),
                        );
                      }).toList()),
              ),
            )
          ],
        );
      },
    );
  }
}
