import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moovle/resources/colors.dart';

typedef ResultComposer<T> = T Function();

class CloseButton extends StatelessWidget {
  final ResultComposer resultComposer;

  const CloseButton({Key key, this.resultComposer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      child: SvgPicture.asset(
        'assets/images/svg/close.svg',
        color: AppColors.DARK_GRAY_COLOR,
      ),
      onPressed: () {
        if (resultComposer != null) {
          Navigator.maybePop(context, resultComposer());
        } else {
          Navigator.maybePop(context);
        }
      },
    );
  }
}
