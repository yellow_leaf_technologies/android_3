import 'package:flutter/cupertino.dart';

class InfinityList extends StatefulWidget {
  final Function fetchMore;
  final int length;
  final bool hasMorePages;
  final Widget Function(int index) builder;

  const InfinityList(
      {Key key, this.fetchMore, this.length, this.hasMorePages, this.builder})
      : super(key: key);

  @override
  _InfinityListState createState() => _InfinityListState();
}

class _InfinityListState extends State<InfinityList> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 50.0;

  _InfinityListState() {
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return index >= widget.length
            ? Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 40),
                child: CupertinoActivityIndicator(),
              )
            : widget.builder(index);
      },
      itemCount: widget.hasMorePages ? widget.length + 1 : widget.length,
      controller: _scrollController,
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      widget.fetchMore();
    }
  }
}
