import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';

class MoovleTextField extends StatelessWidget {
  final String title;
  final String hint;
  final FormFieldValidator<String> validator;
  final TextInputType textInputType;
  final TextEditingController controller;
  final int maxLines;

  MoovleTextField(this.title, this.hint, this.textInputType,
      {this.validator, this.controller, this.maxLines = 1});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5),
            child: TextFormField(
              controller: controller,
              maxLines: maxLines,
              decoration: InputDecoration(
                hintText: hint,
                hintStyle: TextStyle(color: AppColors.INPUT_GRAY_COLOR),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.INPUT_GRAY_COLOR, width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.THEME_COLOR, width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: CupertinoColors.destructiveRed, width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: CupertinoColors.destructiveRed, width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
              ),
              keyboardType: textInputType,
              cursorColor: AppColors.THEME_COLOR,
              validator: validator,
            ),
          ),
        ],
      ),
    );
  }
}
