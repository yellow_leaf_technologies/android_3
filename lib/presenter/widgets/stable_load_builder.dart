import 'package:artemis/artemis.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:graphql/internal.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/services/app_localizations.dart';

typedef BuilderCallback = Widget Function(
    Map data, Function(FetchMoreOptions fetchMoreOptions) fetchMore,
    {bool loading});

class StableLoadBuilder extends StatefulWidget {
  final GraphQLQuery query;
  final Map<String, dynamic> args;
  final BuilderCallback builder;
  final bool stabilize;

  const StableLoadBuilder(
      {Key key,
      @required this.query,
      @required this.builder,
      this.args,
      this.stabilize = true})
      : super(key: key);

  @override
  _StableLoadBuilderState createState() => _StableLoadBuilderState();
}

class _StableLoadBuilderState extends State<StableLoadBuilder> {
  ObservableQuery observableQuery;

  void _initQuery() {
    final GraphQLClient client = GraphQLProvider.of(context).value;
    assert(client != null);

    var _options = WatchQueryOptions(
      documentNode: widget.query.document,
      variables: widget.args,
      fetchResults: true,
    );

    observableQuery?.close();
    observableQuery = client.watchQuery(_options);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (observableQuery == null) {
      _initQuery();
    }
  }

  @override
  void didUpdateWidget(StableLoadBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!widget.stabilize ||
        (observableQuery == null ||
            !MapEquality().equals(widget.args, oldWidget.args))) {
      _initQuery();
    }
  }

  @override
  void dispose() {
    observableQuery?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QueryResult>(
      initialData: QueryResult(loading: true),
      stream: observableQuery.stream,
      builder: (
        BuildContext buildContext,
        AsyncSnapshot<QueryResult> snapshot,
      ) {
        var result = snapshot.data;
        var refetch = observableQuery.refetch;

        if (result.hasException) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: <Widget>[
                Text(AppLocalizations.of(context)
                    .translation
                    .something_went_wrong),
                CupertinoButton(
                  child: Icon(CupertinoIcons.restart),
                  onPressed: () {
                    refetch();
                  },
                ),
              ],
            ),
          );
        }

        if (result.data == null && result.loading) {
          return CupertinoActivityIndicator();
        }

        return widget.builder(result.data, observableQuery.fetchMore,
            loading: result.loading);
      },
    );
  }
}

class ExceptionView extends StatelessWidget {
  final VoidCallback refetch;

  const ExceptionView({Key key, this.refetch}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: <Widget>[
          Text(AppLocalizations.of(context).translation.something_went_wrong),
          CupertinoButton(
            child: Icon(CupertinoIcons.restart),
            onPressed: () {
              refetch();
            },
          ),
        ],
      ),
    );
  }
}
