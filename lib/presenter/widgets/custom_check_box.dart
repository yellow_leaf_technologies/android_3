import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';

class CustomCheckBox extends StatelessWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;
  final bool tristate;
  final MaterialTapTargetSize materialTapTargetSize;

  CustomCheckBox({
    Key key,
    @required this.value,
    this.tristate = false,
    @required this.onChanged,
    this.activeColor,
    this.materialTapTargetSize,
  })  : assert(tristate != null),
        assert(tristate || value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      clipBehavior: Clip.hardEdge,
      borderRadius: BorderRadius.all(Radius.circular(3)),
      child: SizedBox(
        width: Checkbox.width,
        height: Checkbox.width,
        child: Container(
            width: Checkbox.width,
            height: Checkbox.width,
            decoration: new BoxDecoration(
              border: Border.all(
                  width: value ? 0 : 2,
                  color: AppColors.GREY_LINE ??
                      Theme.of(context).disabledColor),
              borderRadius: new BorderRadius.circular(3),
            ),
            child: Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.transparent,
              ),
              child: Checkbox(
                value: value,
                tristate: tristate,
                onChanged: onChanged,
                activeColor: AppColors.THEME_COLOR,
                checkColor: AppColors.WHITE_COLOR,
                materialTapTargetSize: materialTapTargetSize,
              ),
            )),
      ),
    );
  }
}
