import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';

class MoovleButton extends StatelessWidget {
  final String text;
  final VoidCallback voidCallback;
  final bool loading;

  MoovleButton(this.text, this.voidCallback, {this.loading = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width, // match parent(all screen)
      height: 50,

      child: RaisedButton(
        elevation: 0,
        textColor: AppColors.WHITE_COLOR,
        color: AppColors.THEME_COLOR,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: loading
            ? CupertinoActivityIndicator()
            : Text(text, style: TextStyle(fontSize: 17, color: AppColors.WHITE_COLOR)),
        onPressed: loading ? null : voidCallback,
      ),
    );
  }
}
