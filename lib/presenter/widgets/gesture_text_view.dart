import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';

class GestureTextView extends StatelessWidget {
  final String text;
  final GestureTapCallback gestureTapCallback;

  GestureTextView(this.text, this.gestureTapCallback);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: gestureTapCallback,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 14,
            color: AppColors.THEME_COLOR,
          ),
        ),
      ),
    );
  }
}
