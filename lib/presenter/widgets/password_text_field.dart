import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class PasswordTextField extends StatefulWidget {
  final String hint;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;

  PasswordTextField(this.hint, {this.controller, this.validator});

  @override
  State<StatefulWidget> createState() {
    return PasswordTextFieldState();
  }
}

class PasswordTextFieldState extends State<PasswordTextField> {
  bool currentPasswordVisible = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              AppLocalizations.of(context).translation.password,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5),
            child: TextFormField(
                controller: widget.controller,
                obscureText: currentPasswordVisible,
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: Icon(
                      currentPasswordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: AppColors.THEME_COLOR,
                    ),
                    onPressed: () {
                      setState(() {
                        currentPasswordVisible = !currentPasswordVisible;
                      });
                    },
                  ),
                  hintText: widget.hint,
                  hintStyle: TextStyle(color: AppColors.INPUT_GRAY_COLOR),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.INPUT_GRAY_COLOR, width: 2),
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.THEME_COLOR, width: 2),
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: CupertinoColors.destructiveRed, width: 2),
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: CupertinoColors.destructiveRed, width: 2),
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                ),
                cursorColor: AppColors.THEME_COLOR,
                validator: widget.validator),
          ),
        ],
      ),
    );
  }
}
