import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gql/ast.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/services/auth.dart';

class SaveButton extends StatefulWidget {
  final String id;
  final bool saved;

  const SaveButton({Key key, this.id, this.saved}) : super(key: key);

  @override
  _SaveButtonState createState() => _SaveButtonState();
}

class _SaveButtonState extends State<SaveButton> {
  bool saved;
  Auth auth;
  DocumentNode saveMutation = SaveMutation().document;
  DocumentNode unsaveMutation = UnsaveMutation().document;

  @override
  void initState() {
    super.initState();
    saved = widget.saved;
    Auth.instance.then((instance) {
      if (mounted) {
        setState(() {
          auth = instance;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (auth == null || !auth.isAuthenticated) {
      return Container();
    }

    return Mutation(
      options: MutationOptions(
        documentNode: saved ? unsaveMutation : saveMutation,
        onCompleted: (dynamic resultData) async {
          if (resultData != null && mounted) {
            setState(() {
              saved = !saved;
            });
          }
        },
        onError: (exception) {
          print(exception);
        },
      ),
      builder: (
        RunMutation runMutation,
        QueryResult result,
      ) {
        if (result.loading) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 18, vertical: 18),
            alignment: Alignment.topRight,
            child: CupertinoActivityIndicator(),
          );
        }

        return Container(
          alignment: Alignment.topRight,
          child: CupertinoButton(
            child: saved
                ? SvgPicture.asset('assets/images/svg/ic_heart_active.svg')
                : SvgPicture.asset('assets/images/svg/ic_heart_inactive.svg'),
            onPressed: () {
              var args = saved
                  ? UnsaveArguments(listing_id: widget.id).toJson()
                  : SaveArguments(listing_id: widget.id).toJson();
              runMutation(args);
            },
          ),
        );
      },
    );
  }
}
