import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:moovle/resources/colors.dart';

class RangeSliderData extends StatelessWidget {
  double min;
  double max;
  double lowerValue;
  double upperValue;
  int divisions;
  bool showValueIndicator;
  int valueIndicatorMaxDecimals;
  bool forceValueIndicator;
  frs.RangeSliderCallback callback;

  RangeSliderData(
      {this.min,
      this.max,
      this.lowerValue,
      this.upperValue,
      this.divisions,
      this.showValueIndicator: true,
      this.valueIndicatorMaxDecimals: 1,
      this.forceValueIndicator: false,
      this.callback});

  String get lowerValueText =>
      lowerValue.toStringAsFixed(valueIndicatorMaxDecimals);
  String get upperValueText =>
      upperValue.toStringAsFixed(valueIndicatorMaxDecimals);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Expanded(
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: AppColors.GRAY_TEXT_COLOR,
                activeTickMarkColor: AppColors.GRAY_TEXT_COLOR,
                activeTrackColor: AppColors.GRAY_TEXT_COLOR,
                inactiveTrackColor: AppColors.GREY_LINE,
                trackHeight: 6.0,
                thumbColor: AppColors.GRAY_TEXT_COLOR,
                valueIndicatorColor: AppColors.GRAY_TEXT_COLOR,
                showValueIndicator: showValueIndicator
                    ? ShowValueIndicator.always
                    : ShowValueIndicator.onlyForDiscrete,
              ),
              child: frs.RangeSlider(
                min: min,
                max: max,
                lowerValue: lowerValue,
                upperValue: upperValue,
                divisions: divisions,
                showValueIndicator: showValueIndicator,
                valueIndicatorMaxDecimals: valueIndicatorMaxDecimals,
                onChanged: (double lower, double upper) {
                  callback(lower, upper);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
