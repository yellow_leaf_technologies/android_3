import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';

class SearchField extends StatelessWidget {
  final GestureTapCallback onTap;
  final bool focus;
  final TextEditingController controller;
  final VoidCallback onEditingComplete;
  final FocusNode focusNode;

  const SearchField(
      {Key key,
      this.onTap,
      this.focus = false,
      this.controller,
      this.onEditingComplete,
      this.focusNode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'search',
      child: Container(
        alignment: Alignment.topCenter,
        height: 44,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(22)),
              color: AppColors.WHITE_COLOR,
              border: Border.all(color: AppColors.GRAY_COLOR)),
          height: 44,
          padding: EdgeInsets.only(right: 22),
          child: Row(
            children: <Widget>[
              Container(
                  height: 44,
                  width: 44,
                  child: Icon(
                    CupertinoIcons.search,
                    color: AppColors.GRAY_COLOR,
                  )),
              Expanded(
                child: onTap != null
                    ? GestureDetector(
                        onTap: onTap,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: Text(
                            AppLocalizations.of(context)
                                .translation
                                .find_a_rental,
                            style: TextStyle(color: AppColors.INPUT_GRAY_COLOR),
                          ),
                        ))
                    : CupertinoTextField(
                        focusNode: focusNode,
                        decoration: null,
                        placeholder: AppLocalizations.of(context)
                            .translation
                            .find_a_rental,
                        autofocus: focus,
                        controller: controller,
                        textInputAction: TextInputAction.search,
                        onEditingComplete: onEditingComplete,
                        style: TextStyle(fontFamily: 'Circular'),
                        placeholderStyle: TextStyle(
                            color: AppColors.INPUT_GRAY_COLOR,
                            fontFamily: 'Circular')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
