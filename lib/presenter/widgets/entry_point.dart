import 'package:flutter/cupertino.dart';
import 'package:moovle/presenter/screens/main_tab_view.dart';
import 'package:moovle/presenter/screens/welcome_screen.dart';
import 'package:moovle/services/auth.dart';

class EntryPoint extends StatefulWidget {
  final Auth auth;

  const EntryPoint({Key key, this.auth}) : super(key: key);

  @override
  _EntryPointState createState() => _EntryPointState();
}

class _EntryPointState extends State<EntryPoint> {
  @override
  void initState() {
    super.initState();
    widget.auth.setListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    widget.auth.clearListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.auth.isAuthenticated) {
      return MainTabView();
    }
    return WelcomeScreen();
  }
}
