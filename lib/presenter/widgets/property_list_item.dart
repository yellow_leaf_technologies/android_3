import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:moovle/generated/graphql_api.graphql.dart';
import 'package:moovle/presenter/screens/property_details_screen.dart';
import 'package:moovle/presenter/widgets/save_button.dart';
import 'package:moovle/resources/colors.dart';
import 'package:moovle/services/app_localizations.dart';
import 'package:moovle/services/format_currency.dart';

const maxImageWidth = 328.0;
const maxImageHeight = 205.0;
const widthShrinkCoefficient = 40.0;
const imagePlaceholder = 'assets/images/img/home-placeholder.png';

class PropertyListItem extends StatelessWidget {
  final Home$Query$ListingPaginator$Listing item;

  const PropertyListItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width;
    double height;
    final screenWidth = MediaQuery.of(context).size.width;
    if (screenWidth > maxImageWidth) {
      width = maxImageWidth;
      height = maxImageHeight;
    } else {
      width = screenWidth - widthShrinkCoefficient;
      height = ((width / maxImageWidth) * maxImageHeight).floorToDouble();
    }

    final localization = AppLocalizations.of(context);
    final feature = _composeFeatureDescription(localization);

    Widget photoView;
    if (item.photos == null || item.photos.length == 0) {
      photoView = Image.asset(
        imagePlaceholder,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );
    } else {
      photoView = CachedNetworkImage(
        imageUrl: item.photos.first.url,
        width: width,
        height: height,
        fit: BoxFit.cover,
        placeholder: (context, url) => Image.asset(
          imagePlaceholder,
          width: width,
          height: height,
          fit: BoxFit.cover,
        ),
        errorWidget: (context, url, error) => Image.asset(
          imagePlaceholder,
          width: width,
          height: height,
          fit: BoxFit.cover,
        ),
      );
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4),
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: width,
            height: height,
            child: Stack(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).push(
                        CupertinoPageRoute(
                            builder: (context) =>
                                PropertyDetailsScreen(id: item.id)));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(3),
                        topRight: Radius.circular(3)),
                    child: photoView,
                  ),
                ),
                // TODO need to get this data from server
//                if (item.availableNow)
//                  Container(
//                      decoration: BoxDecoration(
//                          color: AppColors.THEME_COLOR,
//                          borderRadius: BorderRadius.circular(4)),
//                      padding: const EdgeInsets.symmetric(
//                          vertical: 3, horizontal: 5),
//                      margin: const EdgeInsets.all(16.0),
//                      child: Text(
//                        'AVAILABLE NOW',
//                        style: TextStyle(
//                            color: AppColors.WHITE_COLOR,
//                            fontSize: 10,
//                            fontWeight: FontWeight.bold),
//                      )),
                SaveButton(id: item.id, saved: item.isFavorite),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 14, bottom: 2),
            child: Text(
              item.title,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 16, color: AppColors.THEME_COLOR),
            ),
          ),
          if (item.address != null)
            Text(
              item.address,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 12, color: AppColors.GRAY_COLOR),
            ),
          if (feature != '')
            Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                feature,
                style:
                    TextStyle(fontSize: 12, color: AppColors.GRAY_TEXT_COLOR),
              ),
            ),
          Padding(
            padding: const EdgeInsets.only(top: 6),
            child: Text(
              '${formatCurrency(item.price)}',
              style: TextStyle(
                  fontSize: 16,
                  color: AppColors.THEME_COLOR,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  String _composeFeatureDescription(AppLocalizations localization) {
    final features = <String>[];
    if (item.bedrooms != null) {
      features.add(item.bedrooms.toString() +
          ' ' +
          localization.translation.getBedLabel(item.bedrooms));
    }
    if (item.bathrooms != null) {
      features.add(item.bathrooms.toString() +
          ' ' +
          localization.translation.getBathLabel(item.bathrooms));
    }
    if (item.floor_area != null) {
      features.add(item.floor_area.toString() + localization.translation.area);
    }
    return features.join(' | ');
  }
}
