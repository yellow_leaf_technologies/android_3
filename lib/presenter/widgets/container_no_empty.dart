import 'package:flutter/material.dart';
import 'package:moovle/services/app_localizations.dart';

class ContainerNoEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          AppLocalizations.of(context).translation.no_empty,
          style: TextStyle(
              fontSize: 21, color: Colors.black, fontFamily: 'Circular'),
        ),
      ),
    );
  }
}
