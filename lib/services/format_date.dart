import 'package:intl/intl.dart';

DateFormat dateFormatter = DateFormat('dd MMMM');

String formatDate(DateTime date) {
  return dateFormatter.format(date);
}
