import 'package:moovle/services/user_data.dart';

String composeName(UserData userData) {
  var result = '';
  if (userData.user != null) {
    if (userData.user['first_name'] != null) {
      result += userData.user['first_name'];
    }
    if (userData.user['last_name'] != null) {
      if (result.isNotEmpty) {
        result += ' ';
      }
      result += userData.user['last_name'];
    }
  }
  return result;
}
