import 'dart:async';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

class AuthSocialServices {
  var currentUser;
  var facebookSignIn = FacebookLogin();
  AuthSocialStatus status;

  AuthSocialServices(AuthSocialStatus status) {
    this.status = status;
    initData();
  }

  void initData() {
    switch (status) {
      case AuthSocialStatus.google:
        _googleSignIn.onCurrentUserChanged
            .listen((GoogleSignInAccount account) {
          currentUser = account;
          print("--- $account");
        });
        _googleSignIn.signInSilently();
        break;
      default:
    }
  }

  Future getUser() {
    return Future.value(currentUser);
  }

  Future logout() {
    switch (status) {
      case AuthSocialStatus.facebook:
        return _facebooklogOut();
      case AuthSocialStatus.google:
        return _handleSignOut();
    }
    return Future.value(null);
  }

  Future loginUser() {
    switch (status) {
      case AuthSocialStatus.facebook:
        return facebookLogin();
      case AuthSocialStatus.google:
        return googleLogin();
    }
  }

  Future facebookLogin() async {
    var facebookLoginResult =
        await facebookSignIn.logIn(['email', 'public_profile']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("--- Error");
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("--- CancelledByUser");
        break;
      case FacebookLoginStatus.loggedIn:
        print("--- LoggedIn");
        break;
    }
  }

  Future googleLogin() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  Future<Null> _facebooklogOut() => facebookSignIn.logOut();

  Future<Null> _handleSignOut() => _googleSignIn.disconnect();
}

enum AuthSocialStatus {
  facebook,
  google,
}
