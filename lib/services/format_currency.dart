import 'package:intl/intl.dart';

final currencyFormatter = new NumberFormat.simpleCurrency(name: 'EUR');

String formatCurrency(num number) {
  return currencyFormatter.format(number);
}
