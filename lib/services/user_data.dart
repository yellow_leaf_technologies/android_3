import 'package:hive/hive.dart';
import 'package:moovle/services/auth.dart';

class UserData {
  static const defaultId = 'anonymous';
  static const limitCount = 20;

  static UserData _instance;
  static Future<UserData> get instance async {
    if (_instance != null) {
      return _instance;
    } else {
      var history = await Hive.openBox<List<String>>('history');
      var filters = await Hive.openBox<Map<dynamic, dynamic>>('filters');
      var user = await Hive.openBox<Map<dynamic, dynamic>>('user');
      _instance = UserData._internal(history, filters, user);
      await _instance.init();
      return _instance;
    }
  }

  String _id;
  Box<List<String>> _historyBox;
  // TODO Hive doesn't work with Map<String, dynamic>, so we should count on key type
  Box<Map<dynamic, dynamic>> _filtersBox;
  Box<Map<dynamic, dynamic>> _userBox;

  UserData._internal(this._historyBox, this._filtersBox, this._userBox);

  Future init() async {
    var auth = await Auth.instance;
    if (!auth.isAuthenticated) {
      _id = defaultId;
    } else {
      _id = auth.id;
    }
  }

  List<String> get history => _historyBox.get(_id) ?? List<String>();
  Future addToHistory(String value) async {
    if (value.isEmpty) {
      return;
    }
    var _history = history;
    if (_history.length >= limitCount) {
      _history = _history.sublist(1, limitCount);
    }
    _history.add(value);
    _historyBox.put(_id, _history);
  }

  Map<dynamic, dynamic> get filters =>
      _filtersBox.get(_id) ?? Map<dynamic, dynamic>();
  Future changeFilters(Map<String, dynamic> value) async {
    _filtersBox.put(_id, value);
  }

  Map<dynamic, dynamic> get user => _userBox.get(_id);
  set user(Map<String, dynamic> value) => _userBox.put(_id, value);
}
