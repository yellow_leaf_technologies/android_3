import 'package:intl/intl.dart';

class DefaultTranslation {
  String get an_error_has_occurred => "An error has occurred.";
  String get you_did_not_sign =>
      "You did not sign in correctly. Please try again.";
  String get log_in => "Log in";
  String get please_enter_valid_email => "Please enter valid email";
  String get password_least_character =>
      "Password should be at least 8 character";
  String get log_in_find_home => "Log in and find a home";
  String get forgot_password => "Forgot password?";
  String get unexpected_error_occurred =>
      "Unexpected error occurred. Please try again later.";
  String get ok => "OK";
  String get reset_password => "Reset password";
  String get available_now => "Available now";
  String get pet_friendly => "Pet Friendly";
  String get studios => "Studios";
  String get view_all => "View all";
  String get messaged => "Messaged";
  String get log_out => "Log Out";
  String get make_enquiry => "Make enquiry";
  String get enquiry_made => "Enquiry made";
  String get more_properties_area => "More properties in this area";
  String get more_amenities => "More amenities…";
  String get less_amenities => "Show less";
  String get signed_up_successfully =>
      "You are signed up successfully. Please Log in.";
  String get did_not_sign_up =>
      "You did not sign up correctly. Please try again.";
  String get sign_up_to_moovle => "Sign up to Moovle";
  String get first_name => "First name";
  String get enter_your_first_name => "Enter your first name";
  String get please_fill_out_the_field => "Please fill out the field";
  String get surname => "Surname";
  String get enter_your_surname => "Enter your surname";
  String get enter_your_password => "Enter your password";
  String get already_have_an_account => "Already have an account?";
  String get lets_find_home => "Let's find you a home";
  String get to_get_create_an_account_desc =>
      "To get even more out of Moovle,\n create an account or sign in.";
  String get sign_up => "Sign up";
  String get something_went_wrong => "Something went wrong. Try again later";
  String get password => "Password";
  String get find_what_you_looking => "Find just what you’re looking for";
  String get budget => "Budget";
  String get luxurious => "Luxurious";
  String get furnished => "Furnished";
  String get couples => "Couples";
  String get offices => "Offices";
  String get recent_searches => "RECENT SEARCHES";
  String get search_results => "SEARCH RESULTS";
  String get location => "Location";
  String get area => "m²";
  String get make_an_enquiry => "Make an enquiry";
  String get name => "Name";
  String get phone_number => "Phone number";
  String get enter_phone_number => "Enter Phone number";
  String get please_send_me_more =>
      "Please send me more information on Rental ID ";
  String get enquiry_sent => "Enquiry sent successfully";
  String get success => "Success";
  String get my_properties => "My properties";
  String get saved => "Saved";
  String get security => "Security";
  String get find_a_rental => "Find a rental";
  String get email_address => "Email address";
  String get email_placeholder => "me@email.com";
  String get filter_your_search => "Filter your search";
  String get price_range => "Price range";
  String get property_type => "Property type";
  String get bedroooms => "Bedroooms";
  String get bathrooms => "Bathrooms";
  String get parking => "Parking";
  String get furniture => "Furniture";
  String get pets => "Pets";
  String get features => "Features";
  String get any => "Any";
  String get apartment => "Apartment";
  String get house => "House";
  String get studio => "Studio";
  String get one => "1";
  String get two => "2";
  String get three => "3";
  String get four_plus => "4+";
  String get garage => "Garage";
  String get off_street => "Off-street";
  String get street => "Street";
  String get unfurnished => "Unfurnished";
  String get dog => "Dog(s)";
  String get cat => "Cat(s)";
  String get three_plus => "3+";
  String get no_empty => "Empty";
  String get yes => "Yes";
  String get no => "No";
  String get please_login => "Please Login";
  String get message => "Message";
  String get matches => "Matches";

  String getBedLabel(num count) =>
      Intl.plural(count, one: 'bed', other: 'beds');
  String getBathLabel(num count) =>
      Intl.plural(count, one: 'bath', other: 'baths');
}
