import 'package:intl/intl.dart';
import 'package:moovle/services/i18n/default_translation.dart';

class TranslationNl extends DefaultTranslation {
  String get an_error_has_occurred => "Er heeft zich een fout voorgedaan.";
  String get you_did_not_sign =>
      "U bent niet correct ingelogd. Probeer opniuw.";
  String get log_in => "Inloggen";
  String get please_enter_valid_email => "Vul een geldig e-mail adres in";
  String get password_least_character =>
      "Het wachtwoord moet minstens 8 letters bevatten";
  String get log_in_find_home => "Inloggen";
  String get forgot_password => "Wachtwoord vergeten??";
  String get unexpected_error_occurred =>
      "Er heeft zich een fout voorgedaan. Probeer later opnieuw.";
  String get ok => "OK";
  String get reset_password => "Wachtwoord resetten";
  String get available_now => "Beschikbaar";
  String get pet_friendly => "Huisdier vriendelijk";
  String get studios => "Studios";
  String get view_all => "Allemaal bekijken";
  String get messaged => "Berichten";
  String get log_out => "Uitloggen";
  String get make_enquiry => "Afspraak maken";
  String get more_properties_area => "Meer vastgoed in deze regio";
  String get more_amenities => "Meer voorzieningen…";
  String get less_amenities => "Minder tonen";
  String get signed_up_successfully =>
      "U bent succesvol aangemeld. Log alsjeblieft in.";
  String get did_not_sign_up =>
      "U heeft zich niet correct aangemeld. Probeer het alstublieft opnieuw.";
  String get sign_up_to_moovle => "Meld je aan bij Moovle";
  String get first_name => "Voornaam";
  String get enter_your_first_name => "Wat is jouw voornaam";
  String get please_fill_out_the_field => "Gelieve het veld in te vullen";
  String get surname => "Achternaam";
  String get enter_your_surname => "Wat is jouw achternaam";
  String get enter_your_password => "Wachtwoord";
  String get already_have_an_account => "Heb je al een account?";
  String get lets_find_home => "Vind jouw droomwoning";
  String get to_get_create_an_account_desc =>
      "Om nog meer uit Moovle te halen,\n Creëer een account of login.";
  String get sign_up => "Aanmelden";
  String get something_went_wrong =>
      "Er is iets fout gegaan. Probeer het later nog eens";
  String get password => "Wachtwoord";
  String get find_what_you_looking => "Vind precies wat je zoekt";
  String get budget => "Budget";
  String get luxurious => "Luxueus";
  String get furnished => "Gemeubeld";
  String get couples => "Koppels";
  String get offices => "Kantoorruimtes";
  String get recent_searches => "Recente zoekopdrachten";
  String get search_results => "Recente resultaten";
  String get location => "locatie";
  String get area => "m²";
  String get make_an_enquiry => "Ontvang meer informatie";
  String get name => "Naam";
  String get phone_number => "Telefoonnummer";
  String get enter_phone_number => "Vul hier jouw telefoonnummer";
  String get please_send_me_more =>
      "Graag wil ik meer informatie ontvangen over dit pand";
  String get enquiry_sent => "Aanvraag succesvol verzonden";
  String get success => "Success";
  String get my_properties => "Mijn panden";
  String get saved => "Saved";
  String get security => "Security";
  String get find_a_rental => "Vind een pand";
  String get email_address => "Email address";
  String get email_placeholder => "me@email.com";
  String get filter_your_search => "Filter uw zoekopdracht";
  String get price_range => "Prijsbereik";
  String get property_type => "Vastgoed type";
  String get bedroooms => "Slaapkamer";
  String get bathrooms => "Slaapkamers";
  String get parking => "Parking";
  String get furniture => "Bemeubeld";
  String get pets => "Huisdieren";
  String get features => "Kenmerken";
  String get any => "Any";
  String get apartment => "Appartement";
  String get house => "Woning";
  String get studio => "Studio";
  String get one => "1";
  String get two => "2";
  String get three => "3";
  String get four_plus => "4+";
  String get garage => "Garage";
  String get off_street => "Op-straat";
  String get street => "Straat";
  String get unfurnished => "Niet gemeubeld";
  String get dog => "Hond";
  String get cat => "Kat";
  String get three_plus => "3+";
  String get no_empty => "Empty";
  String get yes => "Ja";
  String get no => "Nee";
  String get please_login => "Inloggen";

  String getBedLabel(num count) =>
      Intl.plural(count, one: 'slaapkamer', other: 'slaapkamers');
  String getBathLabel(num count) =>
      Intl.plural(count, one: 'badkamer', other: 'badkamers');
}
