import 'package:flutter/cupertino.dart';

// Height and Width of image assets/images/img/homepage.jpg
class HomeImage {
  static double maxImageHeight(BuildContext context) =>
      1414 / MediaQuery.of(context).devicePixelRatio;
  static double maxImageWidth(BuildContext context) =>
      2121 / MediaQuery.of(context).devicePixelRatio;
}
