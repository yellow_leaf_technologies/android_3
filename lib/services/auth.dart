import 'package:jwt_decode/jwt_decode.dart';
import 'package:moovle/generated/graphql_api.dart';
import 'package:moovle/services/user_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Auth {
  static Auth _instance;
  static Future<Auth> get instance async {
    if (_instance != null) {
      return _instance;
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token');
      String id = prefs.getString('id');
      DateTime exp;
      if (token != null) {
        var payload = Jwt.parseJwt(token);
        if (payload['exp'] != null) {
          exp = DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000);
        }
      }
      _instance = Auth._internal(token, id, exp);
      return _instance;
    }
  }

  String _token;
  String _id;
  DateTime _expired;
  Auth._internal(this._token, this._id, this._expired);

  String get token => _token;
  String get id => _id;
  DateTime get expired => _expired;

  Future reset() => setData(token: null, user: null);
  Future setData({String token, Login$Mutation$AuthPayload$User user}) async {
    var id = user?.id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = token;
    if (token != null) {
      var payload = Jwt.parseJwt(token);
      if (payload['exp'] != null) {
        _expired = DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000);
      }
    } else {
      _expired = null;
    }
    _id = id;
    await prefs.setString('token', token);
    await prefs.setString('id', id);
    var userData = await UserData.instance;
    await userData.init();
    if (user != null) {
      userData.user = {
        'first_name': user?.first_name,
        'last_name': user?.last_name,
        'email': user?.email,
        'phone': user?.mobile_phone,
        'avatar': user?.avatar,
      };
    }
    if (_listener != null) {
      _listener();
    }
  }

  bool get isAuthenticated => _token != null;

  Function _listener;

  void setListener(Function listener) {
    _listener = listener;
  }

  void clearListener() {
    _listener = null;
  }
}
