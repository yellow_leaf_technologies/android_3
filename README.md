# moovle

Moovle app

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Realise
For build iOS we should use xCode
For build android need to run
```flutter build appbundle```
Resulted bundle will be `/build/app/outputs/bundle/release/app.aab` 

## GraphQL 
### Queries
All queries should be stored in graphql directory, to be able to edit them and run in Android studio:
- install JSGraphQL plugin
- in "GraphQL" tab run: "Endpoints" -> "Default" -> "Get Graph schema from Endpoint"

This will create `moovle.schema.graphql` so plugin can work
config is placed in `.graphqlconfig`

### In dart code
We use `artemis` to generate classes so we can use them as queries
Config is placed in `build.yaml`
To generate classes run:
```flutter pub run build_runner build```
Them will be generated in `generated` folder

### In flutter widgets
We should use `GraphQLProvider` in a root of app, this is done already.
To load data:
```
Query(
    options: QueryOptions(documentNode: ExampleQuery().document, variables: {'id': 1}),
    builder: (QueryResult result, {VoidCallback refetch, FetchMore fetchMore}) {
      if (result.hasException) {
        return Text('Something went wrong. Try again later');
      }
    
      if (result.loading) {
        return CupertinoActivityIndicator();
      }
      return Text(result.data['my_data_key']);
    },
)
``` 

Where
- `ExampleQuery` is generated class
- `{'id': 1}` is parameters
- `result.data` is a map with your data

To mutate data:
```
Mutation(
  options: MutationOptions(
    documentNode: LoginMutation().document,
    // you can update the cache based on results
    update: (Cache cache, QueryResult result) {
      return cache;
    },
    // or do something with the result.data on completion
    onCompleted: (dynamic resultData) {
      print(resultData);
    },
  ),
  builder: (
    RunMutation runMutation,
    QueryResult result,
  ) {
    return FloatingActionButton(
      onPressed: () => runMutation({
        'id': 1,
      }),
      child: Text('Change'),
    );
  },
);
```